<?php
/*
http://localhost:8088/cnc/ej36_verDatos.php 

*/ 

$srv = "localhost";
$usr = "root";
$pwd = "";
$bd = 'pruebas';


$sql = <<<abc
select a.nombre, c.objeto, d.precio
FROM antiguedades c
inner join anticuarios a on c.id_comprador = a.id_anticuario
inner JOIN precios d on d.objeto = c.objeto 
abc;



// OOP
$conn1 = new mysqli($srv, $usr, $pwd, $bd);

if ($conn1->connect_error) {
    die("no conectamos: " . $conn->connect_error);
}
echo "estamos conectados 1<br>";


$txt1 = ''; 
$qry = $conn1->query($sql);

if ($qry->num_rows > 0) {
    while($rs = $qry->fetch_assoc()) {
        $txt1 .= '<tr><td>'.$rs['nombre'].'</td>'.'<td>'.$rs['objeto'].'</td>'.'<td>'.$rs['precio'].'</td></tr>';
    }

} else {
    echo "0 results";
}

$conn1->close(); 



// PROCEDURAL
$conn2 = mysqli_connect($srv, $usr, $pwd, $bd);


if (!$conn2) {
    die("Connection failed: " . mysqli_connect_error());
}
echo "estamos conectados 2<br>";

$txt2 = ''; 
$qry = mysqli_query($conn2, $sql);

if (mysqli_num_rows($qry) > 0) {
    while($rs = $qry->fetch_assoc()) {
        $txt2 .= '<tr><td>'.$rs['nombre'].'</td>'.'<td>'.$rs['objeto'].'</td>'.'<td>'.$rs['precio'].'</td></tr>';
    }
} else {
    echo "0 results";
}

mysqli_close($conn2); 



// PDO::QUERY
try {
		$conn3 = new PDO("mysql:host=$srv;dbname=pruebas", $usr, $pwd);
		$conn3->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		echo "estamos conectados 3<br>";
	  
		$txt3 = '';
		$qry = $conn3->query($sql, PDO::FETCH_ASSOC);	

		if ($qry->rowCount() > 0) {
			while($rs = $qry->fetch()) {
				$txt3 .= '<tr><td>'.$rs['nombre'].'</td>'.'<td>'.$rs['objeto'].'</td>'.'<td>'.$rs['precio'].'</td></tr>';
			}
		} else {
			echo "0 results";
		}
    }
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

$conn3 = null; 



// PDO::PREPARE
try {
		$conn4 = new PDO("mysql:host=$srv;dbname=pruebas", $usr, $pwd);
		$conn4->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		echo "estamos conectados 4<br>";
	  
		$txt4 = '';
		$qry = $conn4->prepare($sql);	
		$qry->setFetchMode(PDO::FETCH_ASSOC);
		//$qry->bindParam(':calories', $calorías, PDO::PARAM_INT);
		//$qry->bindValue(':colour', "%{$color}%");
		$qry->execute();

		if ($qry->rowCount() > 0) {
			while($rs = $qry->fetch()) {
				$txt4 .= '<tr><td>'.$rs['nombre'].'</td>'.'<td>'.$rs['objeto'].'</td>'.'<td>'.$rs['precio'].'</td></tr>';
			}
		} else {
			echo "0 results";
		}
    }
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

$conn4 = null; 
	
?>


<html>
<head>
	<meta charset="UTF-8">
	<title>Ejer. -ej_36_verDatos-</title>
	

<style>
</style>

<script>
</script>


</head>

<body>

<table>
<caption>Inventario (OOP)</caption>
<tr>
	<td>nombre</td>	
	<td>articulo</td>	
	<td>precio</td>			
</tr>
	<?php echo $txt1;?>
</table>

<hr>
<table>
<caption>Inventario (MYSQLI)</caption>
<tr>
	<td>nombre</td>	
	<td>articulo</td>	
	<td>precio</td>			
</tr>
	<?php echo $txt2;?>
</table>

<hr>
<table>
<caption>Inventario (PDO-query)</caption>
<tr>
	<td>nombre</td>	
	<td>articulo</td>	
	<td>precio</td>			
</tr>
	<?php echo $txt3;?>
</table>

<hr>
<table>
<caption>Inventario (PDO-prepare)</caption>
<tr>
	<td>nombre</td>	
	<td>articulo</td>	
	<td>precio</td>			
</tr>
	<?php echo $txt4;?>
</table>


</body>
</html>
