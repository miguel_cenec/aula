<?php
 
interface GetDatosItf {
	public function conectar();
	public function hacerQuery($conn, $sql);
	public function hayFilas($qry);
	public function mostrarDatos($qry);
	//public function grabar();
	//public function modificar();
	//public function borrar();
	public function cerrar($conn);
	public function getTitulo();
}	

/*
para implementar metodos comunes a todas las clases
tambien se podria hacer con trait, pero no permite implement con lo que tendria implements en todas las demas clases
trait funciona como un copy paste, y aqui nos interesa la herencia de la clase abstracta
*/
abstract class GetDatosAbs implements GetDatosItf {

	protected $titulo;
	
	public function mostrarDatos($obj) {
		$ret = '';
		//$ret = $this->mostrarCabecera($obj);
		
		while($rs = $this->readNext($obj)) {
			if ($ret == '') {
				$ret = $this->mostrarCabecera($rs);	
			}	
			$ret .= '<tr>';
			foreach ($rs as $colval) {
				$ret .= '<td>'.$colval.'</td>';
			}		
			$ret .= '</tr>';
		}
		return $ret;
	}
	
	private function mostrarCabecera($obj) {
		$ret = '<tr>';
		foreach ($obj as $colname => $colval) {
			$ret .= '<th>'.$colname.'</th>';
		}		
		$ret .= '</tr>';
		return $ret;
	}
	
	protected function readnext($obj) {
		return $obj->fetch_assoc();				// en pdo es diferente	
	}		

	public function getTitulo() {
		return $this->titulo;
	}		

}	

class GetDatosFnn extends GetDatosAbs {

	private $srv, $usr, $pwd, $bd;

	function __construct($srv, $usr, $pwd, $bd, $tit='') { 
		$this->srv = $srv;
		$this->usr = $usr;
		$this->pwd = $pwd;
		$this->bd = $bd;
		$this->titulo = $tit.' Consulta (FNN)';		
	}
		
	public function conectar() {
		return mysqli_connect($this->srv, $this->usr, $this->pwd, $this->bd);
	}	
	public function hacerQuery($conn, $sql) {
		return mysqli_query($conn, $sql);
	}	
	public function hayFilas($obj) {
		return (mysqli_num_rows($obj) > 0 ? true : false);	
	}	
	public function cerrar($conn) {
		mysqli_close($conn); 	
	}
}


class GetDatosOop extends GetDatosAbs {

	private $srv, $usr, $pwd, $bd;

	function __construct($srv, $usr, $pwd, $bd, $tit='') { 
		$this->srv = $srv;
		$this->usr = $usr;
		$this->pwd = $pwd;
		$this->bd = $bd;
		$this->titulo = $tit.' Consulta (OOP)';
	}
		
	public function conectar() {
		return new mysqli($this->srv, $this->usr, $this->pwd, $this->bd);
	}	
	public function hacerQuery($conn, $sql) {
		return $conn->query($sql);
	}	
	public function hayFilas($obj) {
		return ($obj->num_rows > 0 ? true : false);	
	}	
	public function cerrar($conn) {
		$conn->close(); 	
	}
}

class GetDatosPdo extends GetDatosAbs {

	private $dsn, $usr, $pwd;

	function __construct($dsn, $usr, $pwd, $tit='') { 
		$this->dsn = $dsn;
		$this->usr = $usr;
		$this->pwd = $pwd;
		$this->titulo = $tit.' Consulta (PDO-Query)';
	}
		
	public function conectar() {
		$conn = new PDO($this->dsn, $this->usr, $this->pwd);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $conn;
	}	
	public function hacerQuery($conn, $sql) {
		return $conn->query($sql, PDO::FETCH_ASSOC);
	}	
	public function hayFilas($obj) {
		return ($obj->rowCount() > 0 ? true : false);	
	}	
	public function cerrar($conn) {
		//$conn = null; 		no funcionaria, con (&$conn) tampoco
		// una de las posibles soluciones seria hacer conn un atributo de la clase	
	}

	protected function readnext($obj) {
		return $obj->fetch();
	}		

}

class GetDatosPdo2 extends GetDatosPdo {

	function __construct($dsn, $usr, $pwd, $tit='') { 
		parent::__construct($dsn, $usr, $pwd, $tit);
		$this->titulo = $tit.' Consulta (PDO-Prepare)';
	}
	
	public function hacerQuery($conn, $sql) {
		$qry = $conn->prepare($sql);	
		$qry->setFetchMode(PDO::FETCH_ASSOC);
		$qry->execute();
		return $qry;
	}	
}
	
//funcion polimorfica
function getDatosFn(getDatosItf $obj, $sql) {
	$conn = $obj->conectar();
	$qry = $obj->hacerQuery($conn, $sql);
	if ($obj->hayFilas($qry)) {
		$ret  = $obj->mostrarDatos($qry);
	} else {
		$ret = '<th>No se han encontrado datos</th>';
	}		
	$obj->cerrar($conn);
	return $ret;
}	

// en oop real se haria a través de una clase y no de una funcion
class TablaCenec {
	public static function getDatos(getDatosItf $obj, $sql) {
		$conn = $obj->conectar();
		$qry = $obj->hacerQuery($conn, $sql);
		if ($obj->hayFilas($qry)) {
			$ret  = $obj->mostrarDatos($qry);
		} else {
			$ret = '<th>No se han encontrado datos</th>';
		}		
		$obj->cerrar($conn);
		return $ret;
	}	
}
