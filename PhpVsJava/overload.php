<?php
/*
php no tiene sobrecarga ya que esto es incompatible con los parametros opcionales
hay varias maneras de implementarlas, esta seria una, esta hecha sobre el constructor
pero se podria hacer sobre cualquier otro metodo
*/

Class Ficha {

	public $nombre;
	private $email;

	function __construct() {
		$params			= func_get_args();
		$num_params		= func_num_args();
		$fn_overload	= '__construct'.$num_params;
		if (method_exists($this,$fn_overload)) {
			call_user_func_array(array($this, $fn_overload), $params);
		}		
	}		

	function __construct0() {
		$this->__construct1("no name");
	}
	function __construct1($nombre) {
		$this->__construct2($nombre,"no mail");
	}
	function __construct2($nombre, $email) {
		$this->nombre = $nombre;
		$this->email = $email;
	}

	public function __toString() {
		return $this->nombre.'<br>'.$this->email.'<hr>';	
	}			
}	

$p0 = new Ficha();
$p1 = new Ficha("unoooo");
$p2 = new Ficha("unoooo","dos");

echo $p0;
echo $p1;
echo $p2;
