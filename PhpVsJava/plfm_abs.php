<?php

Abstract Class Animal { abstract function habla(); }

// al igual que java no se puede instanciar, pero si ser un tipo de objeto (ver la funcion)
// abstracta solo con fines didacticos, podria ser concreta
//Class Animal { function habla() { return 'hola';} }

Class Pez		extends Animal { function habla() { return 'gluglu'; } }	
Class Felino	extends Animal { function habla() { return 'gruuu!'; } }	
Class Gato		extends Felino { function habla() { return 'miauu!'; } }	

//funcion polimorfica
function quedice(Animal $obj) {
	$ret = $obj->habla().'<br>';
	return $ret;
}

$bicho1 = new Pez;
$bicho2 = new Gato;

echo quedice($bicho1);			// gluglu	
echo quedice($bicho2);			// miauu!


/*
		Otro ejemplo que quizas muestre mejor su utilidad que con los anlimales
*/ 

class Poligono { function calculo() { return 'El area depende del tipo de poligono'; } }

class Cuadrado		extends Poligono { function calculo() { return 'a=l*l'; } }
class Rectangulo	extends Poligono { function calculo() { return 'a=b*h'; } }
class Triangulo		extends Poligono { function calculo() { return 'a=(b*h)/2'; } }

function area(Poligono $obj) {
	$ret = 'area de un cuadrado : '.$obj->calculo().'<br>';
	return $ret;
}

$cuadrado	= new Cuadrado;
$rectangulo	= new Rectangulo;
$triangulo	= new Triangulo;

echo area($cuadrado);
echo area($rectangulo);
echo area($triangulo);
