class Animal { 
	public String varIns = "Animal - variable de instancia";
	Animal() { 
		System.out.println( "Animal - constructor"); 
		System.out.println( varIns ); 
	}
}
class Felino extends Animal{ 
	public String varIns = "Felino - variable de instancia";
	Felino() { 
		System.out.println( "Felino - constructor"); 
		System.out.println( varIns ); 
	}
}
class Gato extends Felino{ 
	public String varIns = "Gato - variable de instancia";
	Gato() { 
		System.out.println( "Gato - constructor"); 
		System.out.println( varIns ); 
	}
}

public class hrnc_constr {

	public static void main(String[] args) {

		Animal obj1 = new Animal();
		System.out.println( "---------------------"); 
		Felino obj2 = new Felino();
		System.out.println( "---------------------"); 
		Gato obj3 = new Gato();
		System.out.println( "---------------------"); 
		
		queDice(obj1);
		queDice(obj2);
		queDice(obj3);
	}

	static void queDice(Animal sujeto){
		System.out.println(sujeto.varIns);
	}

}

/*


La salida de este programa es muy diferente al de php:
- constructores:  
antes de ejecutar el de la clase concreta, se  ejecutan todos los contructores () (sin parametros)
de la jeraquia de clases hacia abajo. Desde object a la clase concreta.
El tema de los contrutores es bastante más complejo y tiene otras mas caracteristicas en java.
- funcion queDice
En Php aunque la funcion esta tipada (Animal) no se comporta como una oop real.
En java si tiene en cuenta el tipo (forma/atributos) y su comportamiento (metodos)
Desde los metodos del hijo si se accede a sus atributos
Animal obj3 = new Gato()
TIPO   obj3 = new COMPORTAMIENTO()

La salida por consola seria:
   
Animal - constructor
Animal - variable de instancia
---------------------
Animal - constructor
Animal - variable de instancia
Felino - constructor
Felino - variable de instancia
---------------------
Animal - constructor
Animal - variable de instancia
Felino - constructor
Felino - variable de instancia
Gato - constructor
Gato - variable de instancia
---------------------
Animal - variable de instancia
Animal - variable de instancia
Animal - variable de instancia
*/
