interface QueHace {
    String habla();
}

abstract class Animal implements QueHace { public abstract String habla(); }

class Pez		extends Animal { public String habla() { return "gluglu"; } }	
class Felino	extends Animal { public String habla() { return "gruuu!"; } }	
class Gato		extends Felino { public String habla() { return "miauu!"; } }	

class RelojCucu	implements QueHace { public String habla() { return "cucu"; } }


public class plfm_itf {

	public static void main(String[] args) {

		Pez			bicho1	= new Pez(); 
		Animal		bicho2	= new Gato();
		RelojCucu	cosa1	= new RelojCucu();
		QueHace		cosax	= new Felino();
		
		queDice(bicho1);				// gluglo
		queDice(bicho2);				// miauu!
		queDice(cosa1);					// cucu
		queDice(cosax);					// gruuu!

	}

	static void queDice(QueHace sujeto){
		System.out.println(sujeto.habla());
	}
}
