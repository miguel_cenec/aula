<?php
/*
En Php los constructores se heredan mientras que en Java su comportamiento es diferente
*/
Class Animal { 
	public $varIns = 'Animal - variable de instancia';
	function __construct() { echo 'Animal - constructor<br>'.$this->varIns.'<hr>'; }
}
Class Felino extends Animal { 
	public $varIns = 'Felino - variable de instancia';
	function __construct() { echo 'Felino - constructor<br>'.$this->varIns.'<hr>'; }
}
Class Gato extends Felino { 
	public $varIns = 'Gato - variable de instancia';									// (1)
	function __construct() { echo 'Gato - constructor<br>'.$this->varIns.'<hr>'; }		// (2)	
}

function quedice(Animal $obj) {
	$ret = $obj->varIns.'<br>';
	return $ret;
}

	$o1 = new Animal();			// animal x 2
	$o2 = new Felino();			// felino x 2
	$o3 = new Gato();			// gato x 2

	echo queDice($o1);			// animal
	echo queDice($o2);			// felino			(3)
	echo queDice($o3);			// gato				(4)

/*
si se comentan (1) y/o (2) tomaria los herededados de su clase padre (Felino)

(3) - (4)
En java serian las dos Animal ya que la funcion requiere aniaml
En java se accederia a los metodos del hijo y desde esto a sus atributos y no a los del padre
*/
