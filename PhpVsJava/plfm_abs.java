
abstract class Animal { abstract String habla(); }

class Pez		extends Animal { String habla() { return "gluglu"; } }	
class Felino	extends Animal { String habla() { return "gruuu!"; } }	
class Gato		extends Felino { String habla() { return "miauu!"; } }	


public class plfm_abs {

	public static void main(String[] args) {

		Animal bicho1 = new Pez(); 
		Animal bicho2 = new Gato();
		queDice(bicho1);				// gluglo
		queDice(bicho2);				// miauu!
	}

	static void queDice(Animal sujeto){
		System.out.println(sujeto.habla());
	}
}
