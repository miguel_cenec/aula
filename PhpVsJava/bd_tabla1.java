import java.sql.*;

interface GetDatosItf {
	public Connection conectar();
	public ResultSet hacerQuery(Connection conn, String sql);
	public String mostrarDatos(ResultSet rs) throws SQLException;
	public void cerrar(Connection conn) throws SQLException;
	public String getTitulo();
}

abstract class GetDatosAbs implements GetDatosItf {

	protected String titulo;
	private int nrocols;
	
	public String mostrarDatos(ResultSet rs) throws SQLException {
		//StringBuffer es synchronized, StringBuilder no lo es
		StringBuilder ret  = new StringBuilder();

		if (!rs.next()) {
			ret.append("No se han encontrado datos<");
		} else {
			ret.append(mostrarCabecera(rs));
			do {
                for(int i = 1; i <= nrocols; i++) {
                    ret.append(rs.getObject(i) + "\t");
				}
                ret.append("\n");
			} while (rs.next());
		}

		return ret.toString();
	}
	
	private String mostrarCabecera(ResultSet rs) throws SQLException {
		StringBuilder ret  = new StringBuilder();
		ResultSetMetaData metaDatos = rs.getMetaData();
		nrocols = metaDatos.getColumnCount();
		 
		for(int i = 1; i <= nrocols; i++)
			ret.append(metaDatos.getColumnName(i)+"\t");
		ret.append("\n");
		return ret.toString();
	}

	public String getTitulo() {
		return titulo;
	}		

	public void cerrar(Connection conn) throws SQLException {
		conn.close(); 	
	}

}	

class GetDatosMysql extends GetDatosAbs {

	private String srv, usr, pwd, bd;

	GetDatosMysql (String srv, String usr, String pwd, String bd, String tit) { 
		this.srv = srv;
		this.usr = usr;
		this.pwd = pwd;
		this.bd = bd;
		this.titulo = tit + " Consulta (OOP)";
	}
		
	public Connection conectar() {
        Connection conn = null;

        try {
			String url = "jdbc:mysql://" + srv + "/" + bd + "?useSSL=false";
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, usr, pwd);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return conn;		
	}	

	public ResultSet hacerQuery(Connection conn, String sql) {
		ResultSet rs = null;
        try {
			Statement st = conn.createStatement();
			rs = st.executeQuery (sql);		
        } catch (SQLException e) {
            e.printStackTrace();
        }
		return rs;
	}	
/*
 * no hay un metodo directo para saber si el rs esta vacio
	public boolean hayFilas($obj) {
		return ($obj->num_rows > 0 ? true : false);	
	}
 * una alternativa seria metodo que devuelva linea y retorne el cursor al inicio del rs 	
	public int getRows(ResultSet res){
		int totalRows = 0;
		try {
			res.last();
			totalRows = res.getRow();
			res.beforeFirst();
		} 
		catch(Exception ex)  {
			return 0;
		}
		return totalRows ;    
	}
*/
}

public class bd_tabla1 {

    public static void main(String[] args) {

		String sql = String.join(" "
				, "select a.nombre, c.objeto, d.precio"
				, "FROM antiguedades c"
				, "inner join anticuarios a on c.id_comprador = a.id_anticuario"
				, "inner JOIN precios d on d.objeto = c.objeto" 
		);
		
		GetDatosItf mitabla = new GetDatosMysql("localhost","root","","pruebas","Inventario");

		getDatos(mitabla, sql);
    }

	public static void getDatos (GetDatosItf obj, String sql){
		try {
			Connection conn = obj.conectar();
			ResultSet rs = obj.hacerQuery(conn, sql);
			
			String ret  = obj.mostrarDatos(rs);
			obj.cerrar(conn);

			System.out.println(ret); 
        } catch (SQLException e) {
            e.printStackTrace();
        }

	}	
}
