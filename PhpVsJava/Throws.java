
class ThrowsTest {

	public void u1(int a) {
		int b = 15/a;
	}	

	public void u2(int a) throws ArithmeticException {
		int b = 15/a;
	}	

}


public class Throws {

    public static void main(String[] args) {
		ThrowsTest obj = new ThrowsTest();
		
		//obj.u1(0);	
		try {
			obj.u2(0);							// 2 - 3 - 1
			System.out.println("(4) u2");            
        } catch (Exception e) {
            e.printStackTrace();
			System.out.println("(2) catch u2");            
        } finally {
			System.out.println("(3) finally u2");            
		}			


		try {
			dobleT(obj);						// 6 - 7 - 1
			System.out.println("(5) dT");            
        } catch (Exception e) {
			System.out.println("(6) catch dT");            
        } finally {
			System.out.println("(7) finally dT");            
		}			

		System.out.println("(1)----------- Fin ---------------"); 
	}
	

	public static void dobleT(ThrowsTest obj2) throws ArithmeticException {
		obj2.u2(0);
	}	
	
}	
