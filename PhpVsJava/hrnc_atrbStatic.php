<?php
// Late Static Binding

class Animal {
	protected static $bicho = 'Anfibio';
	public static function getBicho1(){
		return self::$bicho;
	}
	public static function getBicho2(){
		return static::$bicho;
	}		
}
class Rana extends Animal {
	protected static $bicho = 'Renacuajo';
}

echo Rana::getBicho1().'<br>';				// Anfibio
echo Rana::getBicho2();						// Renacuajo
