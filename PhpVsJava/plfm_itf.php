<?php
/*
	Con interfaces se tiene mayor nivel de polimorfismo, 
	ya que no depende de una jerarquia de clases (RelojCucu)
*/ 

interface QueHace {
    public function habla();
}


Abstract Class Animal implements QueHace { abstract function habla(); }

Class Pez		extends Animal { function habla() { return 'gluglu'; } }	
Class Felino	extends Animal { function habla() { return 'gruuu!'; } }	
Class Gato		extends Felino { function habla() { return 'miauu!'; } }	

Class RelojCucu	implements QueHace { function habla() { return 'cucu'; } }


//funcion polimorfica
function quedice(QueHace $obj) {
	$ret = $obj->habla().'<br>';
	return $ret;
}

$bicho1 = new Pez;
$bicho2 = new Gato;
$cosa1	= new RelojCucu;

echo quedice($bicho1);			// gluglu	
echo quedice($bicho2);			// miauu!
echo quedice($cosa1);			// cucu


