/*
problema: hijo metEst1 accede a la varEst del padre, como hace que acceda a la del hijo
en java parece que no es posible Late Static Binding, y que habria que rediseñar los objetos
Pdte buscar info de esto en mis notas y en internet
*/
class Padre {
	public static String varEst = "Padre - varible estatica";
	public static void metEst1() {System.out.println("Padre - metodo estatico, accede: "+varEst);}	
	public static void metEst2() {System.out.println("Padre - metodo estatico, accede: "+varEst);}	
}
class Hijo extends Padre{
	public static String varEst = "Hijo - varible estatica";
}

public class hrnc_atrbStatic {

	public static void main(String[] args) {
		System.out.println(Hijo.varEst);			// hijo var
		Hijo.metEst1();								// m:padre v:padre
		Hijo.metEst2();								// m:hijo v:hijo
		Hijo.metEst3();								// m:hijo v:hijo
		
		Hijo obj1 = new Hijo();
		obj1.metEst1();								// m:padre v:padre
		System.out.println(obj1.varEst);			// hijo var
		
	}
}
