class Ficha {

	private String nombre;
	private String email;

	Ficha () {
		this("no name");
	}		

	Ficha (String prmt1) {
		this(prmt1, "no mail");
	}		

	Ficha (String nombre, String prmt2) {
		this.nombre = nombre;
		email = prmt2;
	}
	
    @Override 
    public String toString() {
		return "Nombre: '" + nombre + "', Email: " + email;
    }			
}

public class overload {

	public static void main(String[] args) {

		Ficha f1 = new Ficha();
		Ficha f2 = new Ficha("Juan");
		Ficha f3 = new Ficha("Luis","pp@pp.es");
		
		System.out.println( f1.toString() );
		System.out.println( f2.toString() );
		System.out.println( f3.toString() );				
	}

}
	

