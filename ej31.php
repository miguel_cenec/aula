<?php
/*
http://localhost:8088/cnc/ej31.php 
localhost\intranet\pruebas\cenec\aula/ej31.php
*/

class Cabecera { 
	private $titulo;
	private $alineacion;
	private $backcolor;
	private $color;
	
	function __construct($titulo, $alineacion, $backcolor, $color) {
		$this->titulo = $titulo;		
		$this->alineacion = $alineacion;		
		$this->backcolor = $backcolor;		
		$this->color = $color;		
	}

	function generar() {
		$estilo = 'text-align:'.$this->alineacion;
		$estilo .= ';color: '.$this->color;
		$estilo .= ';background-color: '.$this->backcolor.';';
		$txt = '<h1 style="'.$estilo.'">'.$this->titulo.'</h1>';
		$txt .= 'color: '.$this->color.'<br>';
		$txt .= 'bgcolor: '.$this->backcolor.'<br>';
		$txt .= 'alineacion: '.$this->alineacion.'<br>';
		return $txt;
	}	
}

class MisFnc {
	public static function rndStr($cadena) {
		$valores = explode(",", $cadena);
		return self::rndArr($valores);
	}	

	public static function rndArr($arrai) {
		return $arrai[mt_rand(0, count($arrai) - 1)];
	}	
}
	
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Ejer. -ej_31-</title>
	

<style>
</style>

<script>
</script>


</head>

<body>

<?php

$colores = "red,blue,orange,cyan,yellow,green";
$estilos = "left,right,center";

$estilo = MisFnc::rndStr($estilos);
$color = MisFnc::rndStr($colores);

$bgcolor = $color;
while ($bgcolor == $color) {
	$bgcolor = MisFnc::rndStr($colores);
}

$cabe_pag = new Cabecera("titulo", $estilo, $color, $bgcolor);
echo $cabe_pag->generar();

?>
 <br><br><br>
 <button onclick="window.location.reload()">Probar otra vez</button> 
 
</body>
</html>
