## CSS

- **ej03_css1**: tabla con estilo cebra y cabeceras horizontales y verticales


## JavaScript

- **ej04_js1**: simple uso de document.write("hola<br>")
- **ej04_js2**: idem anterior usando variables
- **ej04_js3**: uso de prompt("meter algo")
- **ej04_js4**: prompt("meter algo") mientras que no sea un número
- **ej05_js1**: meter 3 números y hayar la media aritmética
- **ej05_js2**: meter 2 datos y comprobar si son iguales
- **ej06_js1**: cálculos numéricos
- **ej06_js2**: intercambio de datos entre variables
- **ej06_js3**: comprobar si número es postivo o negativo
- **ej07_js1**: meter 2 nros. si 1ro>2do sumar else dividir
- **ej07_js2**: si número multiplo de 5
- **ej07_js3**: calcula media para apto o no
- **ej07_js4**: número de digitos de un número
- **ej08_js1**: calcular factorial de un numero
- **ej08_js1r**: calcular factorial de un numero (recursivo)
- **ej08_js2**: numeros fibonacci
- **ej08_js3**: array con 20 nro pares y calcule su media
- **ej11_js1**: getElementById - .value - .innerHTML
- **ej11_js2**: idem arriba con 2 textboxs
- **ej11_js3**: idem arriba con otro calculo
- **ej12_js**: calculo vuelos, css + js


## JQuery
- **ej13_jq**: alert onclick button
- **ej14_jq**: cambiar color tr al hacer click
- **ej15_jq**: cambiar todos los td 
- **ej16_jq**: cambiar links
- **ej17_jq1**: add/remove class
- **ej17_jq2**: cambiar color mouseup / mousedown



- **ej33_1**: idem anterior incluyendo trait
- **ej34**: ejemplo derrores en uso clase abstracta (no instacia, sobreescribir sus metodos)
- **ej35**: clase abstracta y herencia
- **herencia_constructor**: diferente java, constructor se hereda (solo se ejecuta uno)
 


Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
