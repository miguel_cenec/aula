<?php
/*
http://localhost:8088/cnc/ej25_1.php 
*/

	$txt = "";
if (isset($_POST['valor1'])) {
	
	$v1 = $_POST['valor1'];
	$v2 = $_POST['valor2'];
	
	$txt = (trim($v1) == "") ? 'Anónimo' : $v1;
	$txt .= '. ';
	
	if (!is_numeric($v2) || trim($v2) == "") {
		$txt .= 'No ha especificado su edad o la ha introducido mal';
	} else {
		$txt .= ($v2>=18) ? 'Es' : 'No es';
		$txt .= ' mayor de edad';
	}	
}

?>

<html>
<head>
	<meta charset="UTF-8">
	<title>Ejer. -ej_25_1-</title>
	

<style>
</style>

<script>
</script>


</head>

<body>


<form action="<?php echo $_SERVER['PHP_SELF']?>"  method="post">
	Nombre: <input type="text" name="valor1">
	<br><br>
	Edad: <input type="text" name="valor2">
	<br><br>
	<input type="submit" value="Submit">
</form>

<br><br><br><br>

<?php echo $txt;?>


</body>
</html>
