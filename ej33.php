<?php
/*
http://localhost:8088/cnc/ej33.php 
*/

Class Empleado {

	private $nombre;
	private $salario;
	private $dias;		
	
	function __construct($v1, $v2, $v3) {
		$this->nombre = $v1;		
		$this->salario = $v2;
		$this->dias = $v3;						
	}

	public function getNombre() {return $this->nombre;}		
	public function setNombre($valor) {$this->nombre = $valor;}		

	public function getSalario() {return $this->salario;}		
	public function setSalario($valor) {$this->salario = $valor;}		

	public function getDias() {return $this->dias;}		
	public function setDias($valor) {$this->dias = $valor;}		

	public function prtInfo() {
		$txt = 'Nombre: '.$this->nombre.'<br>';
		$txt .= 'Salario:'.$this->salario.'<br>';
		$txt .= 'Dias vacaciones:'.$this->dias.'<br>';			
		return $txt;
	}	

}
	
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Ejer. -ej_33-</title>
	

<style>
</style>

<script>
</script>


</head>

<body>

<?php


$o1 = new Empleado('juan', 1500, 22);
$o2 = new Empleado('Luis', 1700, 12);



echo $o1->prtInfo(); 
echo '<br><br><br>';
echo $o2->prtInfo(); 


?>
 <br><br><br>
 <button onclick="window.location.reload()">Probar otra vez</button> 
 
</body>
</html>

