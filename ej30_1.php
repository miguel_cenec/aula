<?php
/*
http://localhost:8088/cnc/ej30_1.php 
*/

class Menu {

	private $opciones = array();	
	
	public function addOption($valor) {
		$this->opciones[] = $valor;	
	}		

	public function showHor() {
		echo $this->formatLink(0);		
	}	

	public function showVer() {
		echo $this->formatLink(1);
	}	


	private function formatLink($estilo) {
		
		$txt = '';
		$sep = ($estilo == 0) ? '<br>' : '&nbsp;&nbsp;&nbsp;';

		foreach ($this->opciones as $op) {
//			$txt .= '<a href="'.$op->url.'">'.$op->txt.'</a>'.$sep;		//( si fueran publicas)
			$txt .= '<a href="'.$op->getUrl().'">'.$op->getTxt().'</a>'.$sep;
		}	
		
		return $txt; 	
	}	

}	


class Opcion {
	private $url;
	private $txt;
	
	function __construct($a, $b) {
		$this->url = $b;
		$this->txt = $a;		
	}
	
	public function getUrl() { return $this->url; } 

	public function getTxt() { return $this->txt; } 
	
}			
?>

<html>
<head>
	<meta charset="UTF-8">
	<title>Ejer. -ej_30_1-</title>
	

<style>
</style>

<script>
</script>


</head>

<body>


<?php

	$opci = [ ['google','http://google.es'], ['yahoo','http://yojoo.es'], ['Msn','http://msn.es'] ];

	//echo '<pre>';print_r($opci);'</pre><hr>';


	$misops = new Menu();

	foreach ($opci as $op) {
		
		$oop = new Opcion($op[0], $op[1]);
		$misops->addOption($oop);
	}	


	$misops->showHor();
	echo '<hr>';
	$misops->showVer();			
?>


</body>
</html>


