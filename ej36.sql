1.- Selecciona el nombre del anticuario que desea comprar una Mesa 

select a.nombre
from ord_compra b
inner join anticuarios a on a.id_anticuario = b.id_anticuario
where b.objeto='mesa';

Bod


2. - Selecciona que objeto quiere comprar el anticuario Jane

select b.objeto
from ord_compra b
inner join anticuarios a on a.id_anticuario = b.id_anticuario
where a.nombre='Jane';

Silla


3. - ¿Cuánto vale el objeto que quiere comprar Jane?

select d.precio
from ord_compra b
inner join anticuarios a on a.id_anticuario = b.id_anticuario
inner join precios d on b.objeto = d.objeto
where a.nombre='Jane';

39


4. - ¿Cuánto se quiere gastar el anticuario llamado Bob?

select sum(d.precio)
from ord_compra b
inner join anticuarios a on a.id_anticuario = b.id_anticuario
inner join precios d on b.objeto = d.objeto
where a.nombre='Bob';

451 = 150 + 301


5. - ¿Qué Anticuario le compró una cama a Bill?

select a.nombre
from antiguedades c
inner join anticuarios a on a.id_anticuario = c.id_comprador
inner join anticuarios x on x.id_anticuario = c.id_vendedor
where c.objeto='cama' and x.nombre = 'Bill'

Sam


6. - ¿Cuántos dineros ha ganado Bill por los objetos que ha vendido?

select sum(d.precio)
from precios d
inner join antiguedades c on d.objeto = c.objeto
inner join anticuarios a on a.id_anticuario = c.id_vendedor
where a.nombre = 'Bill'

174 = 24 + 120 + 30

select sum(d.precio)
from antiguedades c
inner join precios d on d.objeto = c.objeto
inner join anticuarios a on a.id_anticuario = c.id_vendedor
where a.nombre = 'Bill'

/*
Elegí atacar (from) en la primera a precios ya que ahí estan los datos a sumar,
pero habría que evaluar si atacando a antiguedades (la segunda) es más eficiente,
ya que se ataca directamente a los datos buscados pero la suma se calcula sobre enlace a precios
*/


7. -¿Qué cantidad de objetos distintos ha comprado Jane?

select distinct count(c.objeto)
from antiguedades c
inner join anticuarios a on a.id_anticuario = c.id_comprador
where a.nombre = 'Jane'

3

//version para ver los objetos
select distinct c.objeto
from antiguedades c
inner join anticuarios a on a.id_anticuario = c.id_comprador
where a.nombre = 'Jane'


8. - ¿Cuántos objetos distintos ha comprado Jane de más de 119 €?
(cambiado a 119 para que de algun resultado)

select distinct count(c.objeto)
from antiguedades c
inner join anticuarios a on a.id_anticuario = c.id_comprador
inner join precios d on c.objeto = d.objeto
where a.nombre = 'Jane' AND d.precio>119

1


9.- ¿Cuál es el objeto  más caro vendido por Patricia?

select max(d.precio)
from antiguedades c
inner join anticuarios a on a.id_anticuario = c.id_vendedor
inner join precios d on c.objeto = d.objeto
where a.nombre = 'Patricia'

39


10. - Nombre de los anticuarios que han vendido objetos cuyo nombre empiece por e
(cambiado a b% para que de algun resultado)

select distinct(a.nombre)
from antiguedades c
inner join anticuarios a on a.id_anticuario = c.id_vendedor
where a.nombre like 'b%'

Bill, Bob


11. - ¿Qué anticuarios desean comprar un objeto que han vendido previamente?
select a.nombre
from ord_compra b
inner join anticuarios a on a.id_anticuario = b.id_anticuario
inner join antiguedades c on b.objeto = c.objeto
where b.id_anticuario = c.id_vendedor

Bob


12. - Dinero que se ha gastado Sam en objetos que valen menos de 60 euros.

select sum(d.precio)
from antiguedades c
inner join precios  d on d.objeto = c.objeto
inner join anticuarios a on a.id_anticuario = c.id_comprador
where a.nombre = 'Sam' AND d.precio<60

111 = 24 + 48 + 39


13. - Nombre de aquellos anticuarios que poseen un objeto que alguien quiere comprar

select distinct a.nombre
from antiguedades c
inner join anticuarios a on a.id_anticuario = c.id_comprador
inner join ord_compra b on b.objeto = c.objeto

Bill, Patricia, Sam, Bob

// version para ver los propietarios de ogjetos que se quieren comprar
select a.nombre, b.objeto
from antiguedades c
inner join anticuarios a on a.id_anticuario = c.id_comprador
inner join ord_compra b on b.objeto = c.objeto


14. - ¿Cuánto dinero ha ganado Bob en los objetos que ha vendido al anticuario 21?

select sum(d.precio)
from precios d
inner join antiguedades c on d.objeto = c.objeto
inner join anticuarios a on a.id_anticuario = c.id_vendedor
where a.nombre = 'Bob' and id_comprador = 21

144 = 90 + 54

//version para ver los objetos
select d.objeto, d.precio
from precios d
inner join antiguedades c on d.objeto = c.objeto
inner join anticuarios a on a.id_anticuario = c.id_vendedor
where a.nombre = 'Bob' and id_comprador = 21

/*
version atacando a antiguedades
mismo comentarios que pregunta 6
*/
select sum(d.precio)
from antiguedades c
inner join precios d on d.objeto = c.objeto
inner join anticuarios a on a.id_anticuario = c.id_vendedor
where a.nombre = 'Bob' and id_comprador = 21


15. - ¿Cuánto ha ganado Patricia por vender sillas?

select sum(d.precio)
from precios d
inner join antiguedades c on d.objeto = c.objeto
inner join anticuarios a on a.id_anticuario = c.id_vendedor
where a.nombre = 'Patricia' and c.objeto = 'silla'

78
