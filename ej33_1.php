<?php
/*
http://localhost:8088/cnc/ej33_1.php 
*/

Class Empleado {

	private $nombre;
	private $salario;
	private $dias;		
	
	function __construct($v1, $v2, $v3) {
		$this->nombre = $v1;		
		$this->salario = $v2;
		$this->dias = $v3;						
	}

	public function getNombre() {return $this->nombre;}		
	public function setNombre($valor) {$this->nombre = $valor;}		

	public function getSalario() {return $this->salario;}		
	public function setSalario($valor) {$this->salario = $valor;}		

	public function getDias() {return $this->dias;}		
	public function setDias($valor) {$this->dias = $valor;}		

}

trait Rasgo {

	public function prtInfo($obj) {
		$txt = 'Nombre: '.$obj->getNombre().'<br>';
		$txt .= 'Salario:'.$obj->getSalario().'<br>';
		$txt .= 'Dias vacaciones:'.$obj->getDias().'<br>';			
		return $txt;
	}	
}

class TestEmpleado {
	use Rasgo;	

	public $e1;
	public $e2;

	public function __construct() {

		$this->e1 = new Empleado('juan', 1500, 22);
		$this->e2 = new Empleado('Luis', 1700, 12);
		
	}		
}	
	
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Ejer. -ej_33_1-</title>
	

<style>
</style>

<script>
</script>


</head>

<body>

<?php


	$o1 = new TestEmpleado();

	echo $o1->prtInfo($o1->e1); 
	echo $o1->prtInfo($o1->e2); 

?>
 <br><br><br>
 <button onclick="window.location.reload()">Probar otra vez</button> 
 
</body>
</html>
