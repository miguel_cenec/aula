<?php

class Empleado {
	
	private $diasVacaciones;
	private $nombre;
	private $sueldo;
	protected static $numEmpleados;

	public function __construct($v1, $v2) {
		$this->nombre = $v1;	
		$this->sueldo = $v2;
		self::$numEmpleados++;
	}	
	
	public function getNumEmpleados() {
		return self::$numEmpleados;	
	}
	
	public function __get($property){
		if(property_exists($this, $property)) {
			return $this->$property;
		}
	}		
	
	public function __set($property, $value){
		if(property_exists($this, $property)) {
			$this->$property = $value;
		}
	}	
}


class Tecnico extends Empleado {
	private $especialidad;

	public function __construct($v1, $v2, $v3) {
		$this->nombre = $v1;	
		$this->sueldo = $v2;
		$this->especialidad = $v3;
		static::$numEmpleados++;
	}	

	
	public function prtInfo() {
		$txt = 'nombre :'.$this->nombre.'<br>';
		$txt .= 'sueldo :'.$this->sueldo.'<br>';		
		$txt .= 'diasVacaciones :'.$this->diasVacaciones.'<br>';		
		$txt .= 'especialidad :'.$this->especialidad.'<br>';		
		echo $txt;
	}	
	
}		


class Gestor extends Empleado {
	private $presupuesto;

	public function __construct($v1, $v2, $v3) {
		$this->nombre = $v1;	
		$this->sueldo = $v2;
		$this->presupuesto = $v3;
		static::$numEmpleados++;		
	}	
	
	public function prtInfo() {
		$txt = 'nombre :'.$this->nombre.'<br>';
		$txt .= 'sueldo :'.$this->sueldo.'<br>';		
		$txt .= 'diasVacaciones :'.$this->diasVacaciones.'<br>';		
		$txt .= 'presupuesto :'.$this->presupuesto.'<br>';		

		echo $txt;
		
	}	

	public function calcularBonus($extra, $tiempo) {
		return ($extra*$tiempo/100);	
	}	
		
}		

$obj1 = new Empleado("Juan",1500);
$obj2 = new Tecnico("Luis",1600,"informatica");
$obj3 = new Gestor("Jose",1700,3500);


$obj2->prtInfo();
$obj3->prtInfo();

echo Empleado::getNumEmpleados();

?>
