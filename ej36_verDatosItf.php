<?php
/*
http://localhost/intranet/pruebas/cenec/aula/ej36_verDatosItf.php
*/

include('ej36_verDatosItf_inc.php');

$sql = <<<abc
select a.nombre, c.objeto, d.precio
FROM antiguedades c
inner join anticuarios a on c.id_comprador = a.id_anticuario
inner JOIN precios d on d.objeto = c.objeto 
abc;

// des/comentar linea para elegir el tipo de acceso
//$mitabla = new GetDatosOop('localhost','root','','pruebas','Inventario');
//$mitabla = new GetDatosFnn('localhost','root','','pruebas','Inventario');
//$mitabla = new GetDatosPdo('mysql:host=localhost;dbname=pruebas','root','','Inventario');
$mitabla = new GetDatosPdo2('mysql:host=localhost;dbname=pruebas','root','','Inventario');


//$txt1 = getDatosFn($mitabla, $sql)					// desde una funcion
$txt1 = TablaCenec::getDatos($mitabla, $sql);			// con oop metodo de clase

?>
<html>
<head>
	<meta charset="UTF-8">
	<title>Ejer. -ej_36_verDatosItf-</title>
	
<style></style>
<script></script>
</head>
<body>

<table>
<caption><?php echo $mitabla->getTitulo();?></caption>
	<?php echo $txt1;?>
</table>

</body>
</html>
