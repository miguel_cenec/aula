<?php
/*
http://localhost:8088/cnc/ej34.php

Los dos errores son el problema:
- una clase abstrata no se puede instanciar
- una clase hija de una abstracta debe sobreescribir los metodos abstractos 
 
  
*/

Abstract Class Persona {
	private $nombre;
	private $edad;

	abstract function ponDatos();
	abstract function verDatos();
	
}	

Class Empleado extends Persona {
	private $sueldo;

	function getSueldo() {
		return $this->sueldo;	
	}	

	function setSueldo($valor) {
		$this->sueldo = $valor;	
	}	

					
}			
?>

<html>
<head>
	<meta charset="UTF-8">
	<title>Ejer. -ej_34-</title>
	

<style>
</style>

<script>
</script>


</head>

<body>


<?php

	$o1 = new Persona();
		
	$o2 = new Empleado();

?>


</body>
</html>
