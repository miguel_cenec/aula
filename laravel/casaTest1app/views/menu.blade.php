<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Menu Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

		<!-- Bootstrap Core CSS -->
		<link media="all" type="text/css" rel="stylesheet" href="/assets/bootstrap-3.3.7/css/bootstrap.css">
       
   </head>
    <body>

	<div class="container">
		<h1>Intranet</h1>
		<p>Agrupación para maś fácil acceso desde el movil o el escritorio</p>

		<div class="alert alert-info">
		  <strong>P</strong>ruebas
		</div>

		<div class="list-group" id="menu">
		</div>

		<a id='boton' class="btn btn-primary btn-lg btn-block" href="#" role="button">Volver</a>

	</div>

		<script src="/assets/jquery-3.2.1.min.js"></script>
		<script src="/assets/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<script>
$( document ).ready(function() {

	var elementos = [
						[
							['Otros menus'],
							[
								['{{url("/video")}}','App. Video curso Laravel - Cenec'],
								['pruebas','Para pruebas varias'],
								['bladePrueba','pruebas plantillas section stop/show parent yield component slot'],
								['cabelipe1c','CabeLipe1 - datatable con ajax, con yajra'],
								['dtableP3','Prueba3 - datatable con ajax, con yajra'],
								['dtableP2','Prueba2 - datatable con ajax, sin yajra'],
								['dtableP1','Prueba1 - datatable sin ajax, sin yajra'],
								['nombres','Tabla con filtro -2- (mejorando codigo filtros)'],
								['nombresF1','Tabla con filtro -1- (asc-desc no operativos)'],
								['varios/ingles/Niv/Niv_00.html','Niv - menu'],
							]
						],
						[
							['Aplicaciones'],
							[
								['home/web/cms.php','Mis Notas'],
								['varios/ingles/idioma.php','Idioma app v1'],
								['varios/ingles/notes_v2/idioma.php','Idioma app v2'],
							]
						],
						[
							['Pruebas'],
							[
								['varios/ingles/notes_v2/example/index.php','Ejemplo de carga dinámica con scroll'],
								['varios/ingles/Niv/Niv_05_OLD_extraer_dom.html','Ejemplo recorrer y extraer del DOM']
							]
						],
					];

	var txt ='';
	var opc = 0;
	elementos.forEach(function (grupo) {
		txt ='<a href="#" class="list-group-item list-group-item-success">'+grupo[0]+'</a>'; 
		$('#menu').append(txt);
		grupo[1].forEach(function (elemento) {
			var pdte = (elemento[0] == '') ? ' disabled' : '';
			txt ='<a href="'+elemento[0]+'" class="list-group-item list-group-item-action'+pdte+'">'+elemento[1]+'</a>'; 
			$('#menu').append(txt);
			opc++;
		});				

	});				

	$("#boton").text("Varios ("+opc+")");
});

</script>
    </body>
</html>        
