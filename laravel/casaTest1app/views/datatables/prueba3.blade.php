@extends('app')
@section('mis_estilos')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

@stop
@section('content')
<h1 class="text-primary">Lista de Nombres</h1>

<table class="table table-bordered table-striped table-hover" id="prueba1">
  <thead>
    <tr>
        <th class="text-center">Id</th>
        <th class="text-center">Nombre</th>
        <th class="text-center">Dia</th>
        <th class="text-center">Tipo</th>
        <th class="text-center">Fuente</th>
        <th class="text-center">Acciones</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
<br>
@stop


@section('app_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script>
$(document).ready(function(){

    $('#prueba1').DataTable(
    {
        "processing": true,
        "serverSide": true,
        "ajax": "/dtableP3Ajax",
        "columns" : [
            {data: 'id'},
            {data: 'nombre'},
            {data: 'dia'},
            {data: 'tipo'},
            {data: 'fuente'},
        ],
        "columnDefs": [ {
            "targets": -1,
            "data": null,
            "defaultContent": "<button>Click!</button>"
        } ],
        "lengthMenu": [10,15,20,25,50]
    } );
});
</script>
@stop
