<script id="details-template" type="text/x-handlebars-template">
    <div class="label label-info">User @{{ name }}'s Posts</div>
    <table class="table details-table" id="posts-@{{id}}">
        <thead>
            <tr>
                <th>Id</th>
                <th>line</th>
                <th>Fecha</th>
                <th>nombre</th>
                <th>entero</th>
                <th>decimal</th>
                <th>sino</th>
                <th>cbstr</th>
                <th>cbint</th>
                <th>cbtbl</th>
            </tr>
        </thead>
    </table>
</script>


{
    top: 8px;
    left: 4px;
    height: 16px;
    width: 16px;
    display: block;
    position: absolute;
    color: white;
    border: 2px solid white;
    border-radius: 16px;
    text-align: center;
    line-height: 14px;
    box-shadow: 0 0 3px #444;
    box-sizing: content-box;
    content: '+';
    background-color: #31b131;
}


{
    content: '-';
    background-color: #d33333;
}


    var template = Handlebars.compile($("#details-template").html());
    var table = $('#tbcabe').DataTable({
        processing: true,
        serverSide: true,
        ajax: "cabelipe1cAjax",
        columns: [
            {
                "className":      'details-control',
                "orderable":      false,
                "searchable":      false,
                "data":           null,
                "defaultContent": ''
            },
            {data: 'id', name: 'id'},
            {data: 'id_santoral', name: 'id_santoral'},
            {data: 'nombre', name: 'nombre'}
        ],
        order: [[1, 'asc']]
    });

    // Add event listener for opening and closing details
    $('#tbcabe tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var tableId = 'posts-' + row.data().id;

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(template(row.data())).show();
            initTable(tableId, row.data());
            tr.addClass('shown');
            tr.next().find('td').addClass('no-padding bg-gray');
        }
    });

    function initTable(tableId, data) {
        $('#' + tableId).DataTable({
            processing: true,
            serverSide: true,
            ajax: data.details_url,
            columns: [
                { data: 'id_cabe', name: 'id_cabe' },
                { data: 'nro_linea', name: 'nro_linea' },
                { data: 'fecha', name: 'fecha' },
                { data: 'nombre', name: 'nombre' },
                { data: 'nentero', name: 'nentero' },
                { data: 'ndecimal', name: 'ndecimal' },
                { data: 'sino', name: 'sino' },
                { data: 'cbstr', name: 'cbstr' },
                { data: 'cbint', name: 'cbint' },
                { data: 'cbtbl', name: 'cbtbl' }
            ]
        })
    }

