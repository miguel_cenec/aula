@extends('app')
@section('mis_estilos')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
<style>
td.details-control {
    background: url('/assets/images/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('/assets/images/details_close.png') no-repeat center center;
}
</style>
@stop
@section('content')
<h1 class="text-primary">Lista de Cabecera</h1>

<table class="table table-bordered table-striped table-hover" id="tbcabe">
  <thead>
    <tr>
        <th></th>
        <th class="text-center">Id</th>
        <th class="text-center">Id_Santo</th>
        <th class="text-center">Nombre</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
<script id="details-template" type="text/x-handlebars-template">
    <div class="label label-info">User @{{ name }}'s Posts</div>
    <table class="table details-table" id="posts-@{{id}}">
        <thead>
            <tr>
                <th>Id</th>
                <th>line</th>
                <th>Fecha</th>
                <th>nombre</th>
                <th>entero</th>
                <th>decimal</th>
                <th>sino</th>
                <th>cbstr</th>
                <th>cbint</th>
                <th>cbtbl</th>
            </tr>
        </thead>
    </table>
</script>
<br>
@stop


@section('app_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/DataTables.bootstrap.min.js"></script>

<script type="text/javascript" src="{!! asset('assets/handlebars.js') !!}"></script>

<script>
$(document).ready(function(){

    var template = Handlebars.compile($("#details-template").html());

    var table = $('#tbcabe').DataTable(
    {
        "processing": true,
        "serverSide": true,
        "ajax": "/cabelipe1cAjax",
        "columns" : [
            {
                "className":      'details-control',
                "orderable":      false,
                "searchable":      false,
                "data":           null,
                "defaultContent": ''
            },        
            {"data": 'id'},
            {"data": 'id_santoral'},
            {"data": 'nombre'}
        ],
        "lengthMenu": [10,15,20,25,50],
        "order": [[1, 'asc']]
    } );

    // Add event listener for opening and closing details
    $('#tbcabe tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var tableId = 'posts-' + row.data().id;

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(template(row.data())).show();
            initTable(tableId, row.data());
            tr.addClass('shown');
            tr.next().find('td').addClass('no-padding bg-gray');
        }
    });

    function initTable(tableId, data) {
        $('#' + tableId).DataTable({
            processing: true,
            serverSide: true,
            ajax: data.details_url,
            columns: [
                { data: 'id_cabe', name: 'id_cabe' },
                { data: 'nro_linea', name: 'nro_linea' },
                { data: 'fecha', name: 'fecha' },
                { data: 'nombre', name: 'nombre' },
                { data: 'nentero', name: 'nentero' },
                { data: 'ndecimal', name: 'ndecimal' },
                { data: 'sino', name: 'sino' },
                { data: 'cbstr', name: 'cbstr' },
                { data: 'cbint', name: 'cbint' },
                { data: 'cbtbl', name: 'cbtbl' }
            ]
        })
    }


    // para que click quede seleccionada la fila
    // entra en la funcion pero no cambia el color
    // en este pruebas\tablaResponsive\e1\p2_totales.html fuciona
    // no se si es que no esta definida esa clase
    $('#tbcabe tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
    } );

});
</script>
@stop
