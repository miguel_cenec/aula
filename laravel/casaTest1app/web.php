<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// pruebas plantillas section stop/show parent yield component slot
Route::get('bladePrueba', function () {
    return view('bladeHija');
});

// cabelipe datatable conn ajax con yajra
Route::get('cabelipe1c', 'CabeLipe1Controller@cabelipe1c');
Route::get('cabelipe1cAjax', 'CabeLipe1Controller@cabelipe1cAjax');
Route::get('cabelipe1lAjax/{id}', 'CabeLipe1Controller@cabelipe1lAjax');

// datatable conn ajax con yajra
Route::get('dtableP3', 'DataTableController@prueba3');
Route::get('dtableP3Ajax', 'DataTableController@prueba3Ajax');

// datatable conn ajax sin yajra
Route::get('dtableP2', 'DataTableController@prueba2');
Route::get('dtableP2Ajax', 'DataTableController@prueba2Ajax');

// datatable sin ajax sin yajra
Route::get('dtableP1', 'DataTableController@prueba1');

// tabla bootstrap con filtros mejorados
Route::get('nombresF1', 'NombreController@filtro1');

Route::resource('nombres', 'NombreController');

Route::resource('pruebas', 'PruebaController');

Route::get('original', function () {
    return view('welcome');
});

Route::get('/', function () {
    return view('menu');
});

Route::group(['prefix' => 'video'], function () {
    Route::get('/', function () { return view('videoApp/layaouts/master'); });
	Route::get('/login', function () {    return view('videoApp/login'); }); 
	Route::get('/catalogo', 'videoApp\CatalogoController@getIndex');
	Route::get('/catalogo/create', function () { return view('videoApp/create'); });
	Route::get('/catalogo/show/{id}', 'videoApp\CatalogoController@getShow');
	Route::get('/catalogo/edit/{id}', function ($id) { 	return view('videoApp/edit', array('id'=>$id)); });	
    Route::get('/blade/padre', function () { return view('bladePadre'); });
    Route::get('/blade/hija', function () { return view('bladeHija'); });


	//Route::get('/enmedio', function () { return view('videoApp/varias.enmedio'); })->middleware('enmedio');
	// no encontre o no se si se podra lanzar el middleware despues del submit del formulario PDTE!!!!!!!!!!!!!!
	Route::get('/enmedio', function () { return view('videoApp/varias.enmedio'); });
	Route::get('/enmedio-chk',['uses'=>'videoApp\CatalogoController@checkMD','middleware'=>'enmedioMD']);
/*
http://test1.app/video/enmedio-chk?nombre=pp
http://test1.app/video/enmedio-chk?nombre=miguel
*/
	Route::get('/saludo/{name}/{nickname?}', function($name,$nickname=null) { 
		$name=ucfirst($name); 
		if ($nickname) 
			return "Bienvenido {$name},tu apodo es {$nickname}"; 
		else 
			return "Bienvenido {$name},no tienes apodo";
	});
    
});


/*
// ejemplo paso de prmtr a routa
Route::get('colaboradores/{nombre}', function($nombre){
	return "Mostrando el colaborador $nombre";
});

Route::get('agenda/{mes}/{ano}', function($mes, $ano){
	return "Viendo la agenda de $mes de $ano";
});

// pasar prmtr a controlador
Route::get('tienda/productos/{id}','TiendaController@producto');
*/
