<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Nombre;

/*
PDTE:
- cuando vuelve de show pierde los filtros

*/

class NombreController extends Controller
{
	  /**
	  * Muestra la lista de nombres del santoral
	  *
	  * @return Response
	  */

	public function index()
	{

		$datos = new Nombre;		// podria ser tambien = Nombre::query()
		$queries = [];
		$columns = ['filtro1','search', 'sort'];

		foreach ($columns as $column) {
			if (request()->has($column)) {
				// casos especiales por el like o name column <> filtro
				switch ($column) {
					case 'filtro1':
						$datos = $datos->where('nombre', 'like', request('filtro1') . '%');
						break;
					case 'search':
						$datos = $datos->where('nombre', 'like', '%' . request('search') . '%');
						break;
					case 'sort':
						$datos = $datos->orderBy('nombre', request('sort'));
						break;	
					default:
						$datos = $datos->where($column, request($column));
						break;
				}
				$queries[$column] = request($column);
			}	
		}
/*
		//pdte meter en bucle ant. y hace su case especial
		if (request()->has('sort')) {
			$datos = $datos->orderBy('nombre', request('sort'));
			$queries['sort'] = request('sort');
		}	
*/
		$datos = $datos->paginate(10)->appends($queries);


		// ver diferencia de los 2 sistemas
		return view('nombres.index')->with('nombres', $datos);
		//return view('nombres.index', compact('datos'));

	}

	public function filtro1() {	
	
		// primer version de filtro (un simple link)
		if (request()->has('filtro1')) {
			// para campo = dato
			//$nombres = Nombre::where('tipo', 'SAN')->paginate(10);
			// para like	
			// ->appends('nombre', request('filtro1'))
			$nombres = Nombre::where('nombre', 'like', request('filtro1') . '%')->paginate(10)->appends('filtro1', request('filtro1'));
		} elseif (request()->has('search')) {
			$nombres = Nombre::where('nombre', 'like', '%' . request('search') . '%')->paginate(10)->appends('search', request('search'));

		} else { 		
			//$nombres = Nombre::get();
			$nombres = Nombre::paginate(10);
		}

		return view('nombres.indexF1')->with('nombres', $nombres);
	}


	/**
	 * Muestra la moneda seleccionada por id.
	 * @param $Id 
	 * @return Response
	 */
	public function show($Id)
	{
		// Devuelve la moneda seleccionada por id.
		$nombre = Nombre::find($Id);
		return view('nombres.show')->with('nombre', $nombre);
	}
}
