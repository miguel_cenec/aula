<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Cabecera;
use App\Linea;

//use Yajra\Datatables\Facades\Datatables;

class CabeLipe1Controller extends Controller
{
    // datatable con ajax sin yajra
    public function cabelipe1c()
    {
		//$nombres = Nombre::all();
		return view('datatables.cabelipe1c');		
    }

    public function cabelipe1cAjax()
    {
        return datatables()->eloquent(Cabecera::query())
            ->addColumn('details_url', function ($cabecera) {
                return url('cabelipe1lAjax/' . $cabecera->id);
            })
            ->make(true);
    }

    public function cabelipe1lAjax($id)
    {
    	// con eloquent usando relaciones (post seria el metodo que define la relacion)
    	// $posts = User::find($id)->posts();

        $lineas = Linea::where("id_cabe",$id)->get();

        return datatables()->of($lineas)->make(true);
    }

}
