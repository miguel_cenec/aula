<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Nombre;
// necesarios para prueba2 con ajax sin yajra
use App\Providers\MisClases\SSP;


class DataTableController extends Controller
{
    // datatable con ajax sin yajra
    public function prueba3()
    {
		//$nombres = Nombre::all();
		return view('datatables.prueba3');		
    }

    public function prueba3Ajax()
    {
    	return datatables()->eloquent(Nombre::query())->toJson();
    }

    // datatable con ajax sin yajra
    public function prueba2()
    {
    	// no es necesario carga inicial nombres ya que se hace via ajax
		//$nombres = Nombre::all();
		//return view('datatables.prueba2')->with('nombres', $nombres);
		return view('datatables.prueba2');				
    }

    public function prueba2Ajax()
    {
		// DB table to use
		$table = 'santoral';
		 
		// Table's primary key
		$primaryKey = 'id';
		 
		// Array of database columns which should be read and sent back to DataTables.
		// The `db` parameter represents the column name in the database, while the `dt`
		// parameter represents the DataTables column identifier. In this case simple
		// indexes
		$columns = array(
		    array( 'db' => 'id',		  'dt' => 0 ),
		    array( 'db' => 'nombre',      'dt' => 1 ),
		    array( 'db' => 'dia',   	  'dt' => 2 ),
		    array( 'db' => 'tipo',   	  'dt' => 3 ),
		    array( 'db' => 'fuente',      'dt' => 4 ),
		);
		 
		// SQL server connection information
		$sql_details = array(
		    'user' => 'homestead',
		    'pass' => 'secret',
		    'db'   => 'test',
		    'host' => 'localhost'
		);
		 
		$totalizar = '';//'importe'; 
		 
		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 * If you just want to use the basic configuration for DataTables with PHP
		 * server-side, there is no need to edit below this line.
		 */
		 
		echo json_encode(
		    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $totalizar )
		);		    
    } 

    // datatable sin ajax sin yajra
    public function prueba1()
    {
		$nombres = Nombre::all();
		return view('datatables.prueba1')->with('nombres', $nombres);		
    }

}
