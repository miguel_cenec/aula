-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 18-10-2018 a las 19:17:20
-- Versión del servidor: 5.5.24
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `test`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `santoral`
--

CREATE TABLE IF NOT EXISTS `santoral` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `nombre` char(30) DEFAULT NULL,
  `dia` char(8) DEFAULT NULL,
  `tipo` char(3) DEFAULT NULL,
  `fuente` char(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idname` (`nombre`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=787 ;

--
-- Volcado de datos para la tabla `santoral`
--

INSERT INTO `santoral` (`id`, `nombre`, `dia`, `tipo`, `fuente`) VALUES
(1, 'Miguel', '17-11-92', 'ARC', ''),
(2, 'Antonio', '', '', ''),
(3, 'Rafael', '', '', ''),
(4, 'Polifrasio', '', '', ''),
(5, 'Genaro', '', '', ''),
(6, 'Teovaldo', '', '', ''),
(7, 'Porfirio', '', '', ''),
(8, 'Eufrasio', '', '', ''),
(9, 'Calixto', '16-04-92', 'MAR', 'TVE'),
(10, 'Neron', '', '', ''),
(11, 'Tiberio', '', '', ''),
(12, 'Jose Esposo Virgen', '20-03-92', 'SAN', 'CAL'),
(13, 'Luis', '', '', ''),
(14, 'Juan', '', '', ''),
(15, 'Alejandro', '10-03-92', 'SAN', 'SUR'),
(16, 'Adriana', '', '', ''),
(17, 'Americo', '', '', ''),
(18, 'Basilio', '02-03-92', 'SAN', 'SUR'),
(19, 'Eufemia', '20-03-92', 'SAN', 'SUR'),
(20, 'Eufemio', '', '', ''),
(21, 'Higino', '', '', ''),
(22, 'Regino', '', '', ''),
(23, 'Teofilo', '', '', ''),
(24, 'Dionisio', '10-03-92', 'SAN', 'SUR'),
(25, 'Amada', '', '', ''),
(26, 'Magdalena', '', '', ''),
(27, 'Candelaria', '', '', ''),
(28, 'Isaac', '', '', ''),
(29, 'Ulpiano', '', '', ''),
(30, 'Apolonia', '', '', ''),
(31, 'Servino', '', '', ''),
(32, 'Piedad', '', '', ''),
(33, 'Felicitas', '', '', ''),
(34, 'Ascencion', '', '', ''),
(35, 'Tomas', '', '', ''),
(36, 'Francisco', '', '', ''),
(37, 'Paco', '', '', ''),
(38, 'Jesus', '', '', ''),
(39, 'Rufino', '07-04-92', 'MAR', 'TVE'),
(40, 'Carmen', '', '', ''),
(41, 'Carmelo', '', '', ''),
(42, 'Victor', '30-03-92', 'MAR', 'TVE'),
(43, 'Natividad', '', '', ''),
(44, 'Humildad', '', '', ''),
(45, 'Santiago', '22-04-92', 'MAR', 'TVE'),
(46, 'Alfonso', '', '', ''),
(47, 'Alonso', '', '', ''),
(48, 'Andres', '30-11-92', 'SAN', 'D16'),
(49, 'Julio', '12-04-92', 'SAN', 'TVE'),
(50, 'Joaquin', '', '', ''),
(51, 'Soledad', '', '', ''),
(52, 'Manuel', '', '', ''),
(53, 'Ricardo', '03-04-92', 'CON', 'TVE'),
(54, 'Maria', '', '', ''),
(55, 'Pedro de Zu|iga', '02-03-92', 'SAN', 'SUR'),
(56, 'Gloria', '', '', ''),
(57, 'Purificacion', '', '', ''),
(58, 'Julian', '07-03-92', 'SAN', 'SUR'),
(59, 'Elena', '', '', ''),
(60, 'Bienvenida', '', '', ''),
(61, 'Ventura', '', '', ''),
(62, 'Prudencio', '', '', ''),
(63, 'Pablo', '02-03-92', 'SAN', 'SUR'),
(64, 'Santos', '', '', ''),
(65, 'Ignacio', '', '', ''),
(66, 'Argimiro', '', '', ''),
(67, 'Encarnacion', '', '', ''),
(68, 'Modesto', '', '', ''),
(69, 'Alodia', '', '', ''),
(70, 'Daniel', '', '', ''),
(71, 'Urbano', '08-03-92', 'SAN', 'SUR'),
(72, 'Isabel', '', '', ''),
(73, 'Angeles', '', '', ''),
(74, 'Sergio', '', '', ''),
(75, 'Angustias', '', '', ''),
(76, 'Jorge', '23-04-92', 'SAN', 'TVE'),
(77, 'Nunilon', '', '', ''),
(78, 'Adolfo', '', '', ''),
(79, 'Cruz', '', '', ''),
(80, 'Marcial', '13-03-92', 'SAN', 'SUR'),
(81, 'Justo', '', '', ''),
(82, 'Roman', '', '', ''),
(83, 'Reyes', '', '', ''),
(84, 'Jaime', '', '', ''),
(85, 'Ramon', '', '', ''),
(86, 'Eulalio', '', '', ''),
(87, 'Atilano', '', '', ''),
(88, 'Fidel', '', '', ''),
(89, 'Balbino', '', '', ''),
(90, 'Feliciano', '', '', ''),
(91, 'Deotiva', '', '', ''),
(92, 'Javier', '', '', ''),
(93, 'Felix', '03-03-92', 'SAN', 'SUR'),
(94, 'Eleuterio', '', '', ''),
(95, 'Elvira', '', '', ''),
(96, 'Longino', '', '', ''),
(97, 'Eustaquio', '29-03-92', 'BEA', 'TVE'),
(98, 'Sebastian', '20-03-92', 'SAN', 'SUR'),
(99, 'Pilar', '', '', ''),
(100, 'Sonia', '', '', ''),
(101, 'Mario', '', '', ''),
(102, 'Ana', '', '', ''),
(103, 'Adelina', '', '', ''),
(104, 'Cristobalina', '', '', ''),
(105, 'Cristobal', '', '', ''),
(106, 'Nunilona', '', '', ''),
(107, 'Desire', '', '', ''),
(108, 'Amador', '', '', ''),
(109, 'Celestino', '', '', ''),
(110, 'Adelino', '', '', ''),
(111, 'Eliseo', '', '', ''),
(112, 'Clemente', '', '', ''),
(113, 'Benjamin', '31-03-92', 'SAN', 'TVE'),
(114, 'Agustin', '', '', ''),
(115, 'Lorenzo', '', '', ''),
(116, 'Mariano', '', '', ''),
(117, 'Angelia', '', '', ''),
(118, 'Damiana', '', '', ''),
(119, 'Emilio', '', '', ''),
(120, 'Claudio', '', '', ''),
(121, 'Matias', '', '', ''),
(122, 'Saturnino', '07-03-92', 'SAN', 'SUR'),
(123, 'Eduardo', '', '', ''),
(124, 'Gabriel', '', '', ''),
(125, 'Baltasar', '07-01-92', 'SAN', 'ABC'),
(126, 'Melchor', '07-01-92', 'SAN', 'ABC'),
(127, 'Gaspar', '07-01-92', 'SAN', 'ABC'),
(128, 'Carlos', '', '', ''),
(129, 'Federico', '', '', ''),
(130, 'Domingo', '30-03-92', 'MAR', 'TVE'),
(131, 'Abelardo', '', '', ''),
(132, 'Eusebio', '05-03-97', 'SAN', 'CLM'),
(133, 'Felipe', '11-04-92', 'CON', 'TVE'),
(134, 'Inocencio', '14-03-92', 'SAN', 'SUR'),
(135, 'Lazaro', '17-12-92', 'SAN', 'CAL'),
(136, 'Nicasio', '', '', ''),
(137, 'Rogelio', '', '', ''),
(138, 'Alfredo', '', '', ''),
(139, 'Salvador', '', '', ''),
(140, 'Matilde', '14-03-92', 'SAN', 'SUR'),
(141, 'Gerardo', '23-04-92', 'OBI', 'TVE'),
(142, 'Angela de la Cruz', '02-03-92', 'BEA', 'SUR'),
(143, 'Clemencio', '', '', ''),
(144, 'Rosario', '', '', ''),
(145, 'Olivia', '', '', ''),
(146, 'Isidoro', '17-04-92', 'MAR', 'TVE'),
(147, 'Honorio', '', '', ''),
(148, 'Tiburcio', '14-04-92', 'MAR', 'TVE'),
(149, 'Bautista', '', '', ''),
(150, 'Faustino', '', '', ''),
(151, 'Cesar', '', '', ''),
(152, 'Laureano', '', '', ''),
(153, 'Emiliano', '', '', ''),
(154, 'Telesforo', '', '', ''),
(155, 'Primitivo', '', '', ''),
(156, 'Maxima', '08-04-92', 'MAR', 'TVE'),
(157, 'Ovidio', '', '', ''),
(158, 'Arcangel', '', '', ''),
(159, 'Indalecio', '', '', ''),
(160, 'Cristino', '', '', ''),
(161, 'Cristina', '13-03-92', 'SAN', 'SUR'),
(162, 'Eva', '', '', ''),
(163, 'Adan', '', '', ''),
(164, 'Monserrat', '', '', ''),
(165, 'Florencia', '', '', ''),
(166, 'Olvido', '', '', ''),
(167, 'Sabas', '02-12-92', 'SAN', 'CAL'),
(168, 'Arcadio', '', '', ''),
(169, 'Elidio', '', '', ''),
(170, 'Eugenio', '20-03-92', 'SAN', 'SUR'),
(171, 'Nicolas', '21-03-92', 'SAN', 'SUR'),
(172, 'Ruben', '', '', ''),
(173, 'Pepe', '', '', ''),
(174, 'Eleonora', '', '', ''),
(175, 'Belen', '', '', ''),
(176, 'Adela', '24-12-92', 'SAN', 'CAL'),
(177, 'Milagros', '', '', ''),
(178, 'Vicenta', '', '', ''),
(179, 'Vicente Ferrer', '05-04-92', 'CON', 'TVE'),
(180, 'Eulalia', '10-12-92', 'SAN', 'CAL'),
(181, 'Victoria', '', '', ''),
(182, 'Mercedes', '', '', ''),
(183, 'Rocio', '', '', ''),
(184, 'Arturo', '', '', ''),
(185, 'Paloma', '', '', ''),
(186, 'Estrella', '', '', ''),
(187, 'Oscar', '', '', ''),
(188, 'Asdruval', '', '', ''),
(189, 'Epifanio', '07-04-92', 'OBI', 'TVE'),
(190, 'Fernando', '', '', ''),
(191, 'Marisa', '', '', ''),
(192, 'Olga', '', '', ''),
(193, 'Paula', '', '', ''),
(194, 'Lola', '', '', ''),
(195, 'Noemi', '', '', ''),
(196, 'Sabina', '', '', ''),
(197, 'Virgilio', '', '', ''),
(198, 'Virginia', '', '', ''),
(199, 'David', '', '', ''),
(200, 'Ambrosio', '07-12-92', 'SAN', 'CAL'),
(201, 'Diego', '', '', ''),
(202, 'Segismundo', '', '', ''),
(203, 'Rigoberta', '', '', ''),
(204, 'Rigoberto', '', '', ''),
(205, 'Mauricio', '', '', ''),
(206, 'Arsenio', '', '', ''),
(207, 'Aurora', '', '', ''),
(208, 'Anselmo', '03-03-92', 'SAN', 'SUR'),
(209, 'Aurelio', '', '', ''),
(210, 'Alberto', '', '', ''),
(211, 'Fatima', '', '', ''),
(212, 'Lourdes', '', '', ''),
(213, 'Micaela', '', '', ''),
(214, 'Elisenda', '', '', ''),
(215, 'Octavio', '', '', ''),
(216, 'Asuncion', '', '', ''),
(217, 'Zasquia', '', '', ''),
(218, 'Esteban', '26-12-92', 'SAN', 'CAL'),
(219, 'Estela', '', '', ''),
(220, 'Angel', '', '', ''),
(221, 'Lucas', '', '', ''),
(222, 'Esperanza', '', '', ''),
(223, 'Mirian', '', '', ''),
(224, 'Sara', '', '', ''),
(225, 'Raquel', '', '', ''),
(226, 'Monica', '', '', ''),
(227, 'Carolina', '', '', ''),
(228, 'Estefania', '', '', ''),
(229, 'Arancha', '', '', ''),
(230, 'Maximo', '12-04-92', 'SAN', 'TVE'),
(231, 'Clara', '', '', ''),
(232, 'Trinidad', '', '', ''),
(233, 'Leticia', '', '', ''),
(234, 'Luzmarina', '', '', ''),
(235, 'Flor', '', '', ''),
(236, 'Abencio', '', '', ''),
(237, 'Jeremias', '', '', ''),
(238, 'Osvaldo', '', '', ''),
(239, 'Zacarias', '15-03-92', 'SAN', 'SUR'),
(240, 'Simon', '', '', ''),
(241, 'Valentin', '', '', ''),
(242, 'Valerio', '', '', ''),
(243, 'Genoveva', '', '', ''),
(244, 'Veronica', '', '', ''),
(245, 'Valeriano', '15-12-92', 'SAN', 'CAL'),
(246, 'Albina', '', '', ''),
(247, 'Demetrio', '09-04-92', 'MAR', 'TVE'),
(248, 'Luciano', '26-10-95', 'SAN', 'SUR'),
(249, 'Sabino', '13-03-92', 'SAN', 'SUR'),
(250, 'Silvestre', '31-12-92', 'SAN', 'CAL'),
(251, 'Celso', '', '', ''),
(252, 'Raimundo Lulio', '29-03-92', 'SAN', 'D16'),
(253, 'Higinio', '', '', ''),
(254, 'Tatiana', '', '', ''),
(255, 'Hilario', '16-03-92', 'SAN', 'SUR'),
(256, 'Mauro', '', '', ''),
(257, 'Prisca', '', '', ''),
(258, 'Fabian', '', '', ''),
(259, 'Ines', '', '', ''),
(260, 'Timoteo', '09-11-92', 'SAN', 'SUR'),
(261, 'Tito', '26-10-95', 'SAN', 'SUR'),
(262, 'Inocencia', '', '', ''),
(263, 'Blas', '', '', ''),
(264, 'Agueda', '', '', ''),
(265, 'Macarena', '', '', ''),
(266, 'Jeronimo', '', '', ''),
(267, 'Escolastica', '', '', ''),
(268, 'Benedicto', '', '', ''),
(269, 'Juliana', '05-04-92', 'SAN', 'TVE'),
(270, 'Simeon', '', '', ''),
(271, 'Alvaro', '', '', ''),
(272, 'Policarpo', '', '', ''),
(273, 'Victorino', '', '', ''),
(274, 'Nestor', '', '', ''),
(275, 'Rosendo', '01-03-92', 'SAN', 'SUR'),
(276, 'Beatriz', '', '', ''),
(277, 'Emeterio', '03-03-92', 'SAN', 'SUR'),
(278, 'Celedonio', '03-03-92', 'SAN', 'SUR'),
(279, 'Casimiro', '04-03-92', 'SAN', 'CAL'),
(280, 'Perpetua', '07-03-92', 'SAN', 'SUR'),
(281, 'Constantino', '', '', ''),
(282, 'Nicodemo', '', '', ''),
(283, 'Luisa', '', '', ''),
(284, 'Eusebia', '16-03-92', 'SAN', 'SUR'),
(285, 'Patricio', '17-03-92', 'SAN', 'CAL'),
(286, 'Cirilo', '29-03-92', 'SAN', 'TVE'),
(287, 'Bienvenido', '22-03-92', 'SAN', 'SUR'),
(288, 'Toribio de Mogrovejo', '23-03-92', 'SAN', 'SUR'),
(289, 'Segundo', '29-03-92', 'SAN', 'TVE'),
(290, 'Braulio', '', '', ''),
(291, 'Irene', '05-04-92', 'SAN', 'TVE'),
(292, 'Amos', '31-03-92', 'SAN', 'TVE'),
(293, 'Venancio', '', '', ''),
(294, 'Teodora', '', '', ''),
(295, 'Martin', '13-04-92', 'PAP', 'TVE'),
(296, 'Hemenegildo', '', '', ''),
(297, 'Telmo', '', '', ''),
(298, 'Bernadeta', '', '', ''),
(299, 'Bernardo', '', '', ''),
(300, 'Perfecto', '', '', ''),
(301, 'Emma', '', '', ''),
(302, 'Marcos', '25-04-92', 'MAR', 'TVE'),
(303, 'Catalina de Bolonia', '09-03-92', 'SAN', 'SUR'),
(304, 'Pio', '', '', ''),
(305, 'Atanasio', '', '', ''),
(306, 'Ciriaco', '16-03-92', 'SAN', 'CAL'),
(307, 'Luminosa', '', '', ''),
(308, 'Susana', '', '', ''),
(309, 'Pancracio', '03-04-92', 'SAN', 'TVE'),
(310, 'Felicia', '', '', ''),
(311, 'Pascual', '', '', ''),
(312, 'Claudia', '20-03-92', 'SAN', 'SUR'),
(313, 'Desiderio', '', '', ''),
(314, 'Gregorio', '09-03-92', 'SAN', 'SUR'),
(315, 'Evaristo', '26-10-95', 'SAN', 'SUR'),
(316, 'Restituta', '', '', ''),
(317, 'Elias', '21-03-92', 'SAN', 'SUR'),
(318, 'Engracia', '16-04-92', 'MAR', 'TVE'),
(319, 'Ruperto', '27-03-92', 'SAN', 'CAL'),
(320, 'Rodrigo', '13-03-92', 'SAN', 'SUR'),
(321, 'Solomon', '', '', ''),
(322, 'Olegario', '03-06-92', 'SAN', 'CAL'),
(323, 'Leandro', '', '', ''),
(324, 'Nemesio', '', '', ''),
(325, 'Benigno', '03-04-92', 'SAN', 'TVE'),
(326, 'Ildefonso', '', '', ''),
(327, 'Eulogio', '11-03-92', 'SAN', 'CAL'),
(328, 'Justino', '', '', ''),
(329, 'Marcelino', '', '', ''),
(330, 'Norberto', '', '', ''),
(331, 'Bonifacio', '16-03-92', 'SAN', 'SUR'),
(332, 'Candido', '09-03-92', 'SAN', 'SUR'),
(333, 'Maximino', '', '', ''),
(334, 'Efren', '', '', ''),
(335, 'Bernabe', '', '', ''),
(336, 'Mikaela', '', '', ''),
(337, 'Armando', '', '', ''),
(338, 'Ismael', '', '', ''),
(339, 'German', '', '', ''),
(340, 'Silverio', '', '', ''),
(341, 'Romualdo', '', '', ''),
(342, 'Paulino', '', '', ''),
(343, 'Anastasio', '07-01-92', 'SAN', 'ABC'),
(344, 'Guillermo', '20-03-92', 'SAN', 'SUR'),
(345, 'Pelayo', '', '', ''),
(346, 'Esther', '', '', ''),
(347, 'Crescencio', '', '', ''),
(348, 'Fermin', '', '', ''),
(349, 'Adrian', '', '', ''),
(350, 'Beltran', '', '', ''),
(351, 'Benito', '09-03-92', 'SAN', 'SUR'),
(352, 'Enrique', '', '', ''),
(353, 'Camilo', '', '', ''),
(354, 'Buenaventura', '', '', ''),
(355, 'Teodosio', '', '', ''),
(356, 'Aurea', '', '', ''),
(357, 'Marina', '', '', ''),
(358, 'Brigida', '', '', ''),
(359, 'Natalia', '', '', ''),
(360, 'Nazario', '', '', ''),
(361, 'Marta', '', '', ''),
(362, 'Lidia', '', '', ''),
(363, 'Sixto I', '03-04-92', 'PAP', 'TVE'),
(364, 'Cayetano', '', '', ''),
(365, 'Amor', '', '', ''),
(366, 'Viator', '', '', ''),
(367, 'Ponciano', '', '', ''),
(368, 'Hipolito', '', '', ''),
(369, 'Atanasia', '02-03-92', 'SAN', 'SUR'),
(370, 'Jacinto', '', '', ''),
(371, 'Rosa', '23-08-96', '', ''),
(372, 'Bartolome', '', '', ''),
(373, 'Ceferino', '', '', ''),
(374, 'Candida', '', '', ''),
(375, 'Fiacrio', '', '', ''),
(376, 'Gil', '', '', ''),
(377, 'Rosalia', '', '', ''),
(378, 'Justiniano', '21-03-92', 'SAN', 'SUR'),
(379, 'Guadalupe', '', '', ''),
(380, 'Regina', '', '', ''),
(381, 'Crisostomo', '', '', ''),
(382, 'Cornelio', '', '', ''),
(383, 'Cipriano', '10-03-92', 'SAN', 'SUR'),
(384, 'Jenaro', '08-04-92', 'SAN', 'TVE'),
(385, 'Sofia', '', '', ''),
(386, 'Agapito', '16-03-92', 'SAN', 'SUR'),
(387, 'Mateo', '', '', ''),
(388, 'Vidal', '', '', ''),
(389, 'Constancio', '30-11-95', 'SAN', 'D16'),
(390, 'Cosme', '', '', ''),
(391, 'Damian', '12-04-92', 'SAN', 'TVE'),
(392, 'Cleofas', '', '', ''),
(393, 'Wenceslao', '', '', ''),
(394, 'Teresa', '', '', ''),
(395, 'Gala', '', '', ''),
(396, 'Bruno', '', '', ''),
(397, 'Margarita', '', '', ''),
(398, 'Laura', '', '', ''),
(399, 'Hugo', '09-04-92', 'SAN', 'TVE'),
(400, 'Salome', '', '', ''),
(401, 'Capistrano', '', '', ''),
(402, 'Crisanto', '', '', ''),
(403, 'Daria', '26-10-95', 'SAN', 'SUR'),
(404, 'Felicisimo', '26-10-95', 'SAN', 'SUR'),
(405, 'Florencio', '12-04-92', 'ABA', 'TVE'),
(406, 'Judas', '', '', ''),
(407, 'Maximiliano', '', '', ''),
(408, 'Marcelo', '09-04-92', 'MAR', 'TVE'),
(409, 'Ernesto', '07-11-95', 'SAN', 'SUR'),
(410, 'Severo', '', '', ''),
(411, 'Almudena', '', '', ''),
(412, 'Josafat', '', '', ''),
(413, 'Estanislao', '11-04-92', 'CON', 'TVE'),
(414, 'Odon', '', '', ''),
(415, 'Crispin', '', '', ''),
(416, 'Cecilia', '', '', ''),
(417, 'Flora', '', '', ''),
(418, 'Leonardo', '', '', ''),
(419, 'Eloy', '01-12-92', 'SAN', 'CAL'),
(420, 'Bibiana', '01-12-92', 'SAN', 'CAL'),
(421, 'Inmaculada', '08-12-92', 'SAN', 'CAL'),
(422, 'Restituto', '', '', ''),
(423, 'Damaso', '', '', ''),
(424, 'Lucia', '13-12-92', 'SAN', 'CAL'),
(425, 'Ivan', '', '', ''),
(426, 'Flavio', '', '', ''),
(427, 'Amparo', '', '', ''),
(428, 'Ezequiel', '10-04-92', 'MAR', 'TVE'),
(429, 'Dolores', '', '', ''),
(430, 'Fabio', '', '', ''),
(431, 'Edmundo', '', '', ''),
(432, 'Noelia', '', '', ''),
(433, 'Yolanda', '', '', ''),
(434, 'Noe', '', '', ''),
(435, 'Ruth', '', '', ''),
(436, 'Penelope', '', '', ''),
(437, 'Teodulfo', '', '', ''),
(438, 'Lorena', '', '', ''),
(439, 'Tamara', '', '', ''),
(440, 'Barbara', '', '', ''),
(441, 'Barrabas', '', '', ''),
(442, 'Elizardo', '', '', ''),
(443, 'Minerva', '', '', ''),
(444, 'Sigfrido', '', '', ''),
(445, 'Liliana', '', '', ''),
(446, 'Crispulo', '', '', ''),
(447, 'Trifon', '', '', ''),
(448, 'Aristide', '', '', ''),
(449, 'Nora', '', '', ''),
(450, 'Soraya', '', '', ''),
(451, 'Karina', '', '', ''),
(452, 'Herminia', '', '', ''),
(453, 'Herminio', '25-04-92', 'MAR', 'TVE'),
(454, 'Abrahan', '', '', ''),
(455, 'Teodoto', '', '', ''),
(456, 'Agaton', '', '', ''),
(457, 'Alicia', '', '', ''),
(458, 'Ramiro', '', '', ''),
(459, 'Amancio', '01-03-92', 'SAN', 'SUR'),
(460, 'Alociro', '', '', ''),
(461, 'Laudelino', '', '', ''),
(462, 'Ciceron', '', '', ''),
(463, 'Araceli', '', '', ''),
(464, 'Asencio', '', '', ''),
(465, 'Socrates', '', '', ''),
(466, 'Adeo', '', '', ''),
(467, 'Anibal', '', '', ''),
(468, 'Abundio', '14-04-92', 'CON', 'TVE'),
(469, 'Audacio', '', '', ''),
(470, 'Ludolfo', '', '', ''),
(471, 'Marciano', '26-10-95', 'SAN', 'SUR'),
(472, 'Zenon', '12-04-92', 'SAN', 'TVE'),
(473, 'Zeon', '', '', ''),
(474, 'Anunciacion', '', '', ''),
(475, 'Aitor', '', '', ''),
(476, 'Romulo', '24-03-92', 'SAN', 'CAL'),
(477, 'Bromulo', '', '', ''),
(478, 'Cosima', '', '', ''),
(479, 'Leopoldo', '', '', ''),
(480, 'Bricio', '', '', ''),
(481, 'Rufina', '', '', ''),
(482, 'Remedios', '', '', ''),
(483, 'Fabiola', '', '', ''),
(484, 'Cesonia', '', '', ''),
(485, 'Pantaleon', '', '', ''),
(486, 'Germanico', '', '', ''),
(487, 'Gemelo', '', '', ''),
(488, 'Drusila', '', '', ''),
(489, 'Florian', '', '', ''),
(490, 'Florinda', '', '', ''),
(491, 'Florentina', '', '', ''),
(492, 'Fortunata', '', '', ''),
(493, 'Neus', '', '', ''),
(494, 'Ulises', '', '', ''),
(495, 'Patrocinio', '', '', ''),
(496, 'Eligio', '', '', ''),
(497, 'Isacio', '', '', ''),
(498, 'Priscila', '', '', ''),
(499, 'Georgina', '', '', ''),
(500, 'Agripina', '', '', ''),
(501, 'Consolacion', '', '', ''),
(502, 'Consuelo', '', '', ''),
(503, 'Platon', '04-04-92', 'MAR', 'TVE'),
(504, 'Atila', '', '', ''),
(505, 'Diana', '', '', ''),
(506, 'Nieves', '', '', ''),
(507, 'Paris', '', '', ''),
(508, 'Casiano', '', '', ''),
(509, 'Afra', '', '', ''),
(510, 'Valdomero', '', '', ''),
(511, 'Anacleto', '', '', ''),
(512, 'Ranses', '', '', ''),
(513, 'Leocadio', '', '', ''),
(514, 'Precioso', '', '', ''),
(515, 'Pastor', '', '', ''),
(516, 'Concepcion', '', '', ''),
(517, 'Concha', '', '', ''),
(518, 'Abel', '', '', ''),
(519, 'Saturno', '', '', ''),
(520, 'Aniceto', '17-04-92', 'PAP', 'TVE'),
(521, 'Orencio', '', '', ''),
(522, 'Vindemia', '', '', ''),
(523, 'Oracio', '', '', ''),
(524, 'Liborio', '', '', ''),
(525, 'Peregrina', '', '', ''),
(526, 'Segundino', '', '', ''),
(527, 'Servando', '', '', ''),
(528, 'Africa', '', '', ''),
(529, 'Cintia', '', '', ''),
(530, 'Macario', '10-03-92', 'SAN', 'SUR'),
(531, 'Orlando', '', '', ''),
(532, 'Eduviges', '', '', ''),
(533, 'Baza', '', '', ''),
(534, 'Gilberto', '', '', ''),
(535, 'Rebeca', '', '', ''),
(536, 'Alexia', '', '', ''),
(537, 'Ariel', '', '', ''),
(538, 'Queremon', '', '', ''),
(539, 'Nuria', '', '', ''),
(540, 'Arsacio', '', '', ''),
(541, 'Roque', '', '', ''),
(542, 'Librado', '', '', ''),
(543, 'Miron', '', '', ''),
(544, 'Hugolino', '', '', ''),
(545, 'Serafin', '', '', ''),
(546, 'Moltefalso', '', '', ''),
(547, 'Piedrasanta', '', '', ''),
(548, 'Palmira', '', '', ''),
(549, 'Celia', '', '', ''),
(550, 'Tolomeo', '', '', ''),
(551, 'Onofre', '', '', ''),
(552, 'Donato', '01-03-92', 'SAN', 'SUR'),
(553, 'Benicio', '', '', ''),
(554, 'Eliodoro', '', '', ''),
(555, 'Rodolfo', '', '', ''),
(556, 'Victoriano', '23-03-92', 'SAN', 'SUR'),
(557, 'Lino', '', '', ''),
(558, 'Socio', '', '', ''),
(559, 'Tecla', '', '', ''),
(560, 'Raul', '', '', ''),
(561, 'Fuensanta', '', '', ''),
(562, 'Guido', '', '', ''),
(563, 'Macrobio', '', '', ''),
(564, 'Nicomedes', '', '', ''),
(565, 'Emilas', '', '', ''),
(566, 'Panfilo', '', '', ''),
(567, 'Panfucio', '', '', ''),
(568, 'Proto', '', '', ''),
(569, 'Clodovaldo', '', '', ''),
(570, 'Ursula', '', '', ''),
(571, 'Filomena', '', '', ''),
(572, 'Galdino', '', '', ''),
(573, 'Clotilde', '', '', ''),
(574, 'Gines', '', '', ''),
(575, 'Obdulio', '', '', ''),
(576, 'Rufo', '19-04-92', 'MAR', 'TVE'),
(577, 'Cayo', '10-03-92', 'SAN', 'SUR'),
(578, 'Moises', '', '', ''),
(579, 'Gaudencia', '', '', ''),
(580, 'Pamaquio', '', '', ''),
(581, 'Terenciano', '', '', ''),
(582, 'Regulo', '', '', ''),
(583, 'Prisco', '', '', ''),
(584, 'Jairo', '', '', ''),
(585, 'Antolin', '', '', ''),
(586, 'Hermogenes', '25-04-92', 'MAR', 'TVE'),
(587, 'Elpidio', '', '', ''),
(588, 'Sandalo', '', '', ''),
(589, 'Dorotea', '', '', ''),
(590, 'Serapia', '', '', ''),
(591, 'zZeus', '', '', ''),
(592, 'zPerseo', '', '', ''),
(593, 'zAndromeda', '', '', ''),
(594, 'zCasiopea', '', '', ''),
(595, 'zOrfeo', '', '', ''),
(596, 'zEuridice', '', '', ''),
(597, 'zCaliope', '', '', ''),
(598, 'zPersefonte', '', '', ''),
(599, 'zAfrodita', '', '', ''),
(600, 'zTalia', '', '', ''),
(601, 'zTitania', '', '', ''),
(602, 'zOberon', '', '', ''),
(603, 'Cordula', '', '', ''),
(604, 'Heraclio', '', '', ''),
(605, 'Gaudioso', '', '', ''),
(606, 'Rustico', '', '', ''),
(607, 'Cristeta', '', '', ''),
(608, 'Capitolina', '', '', ''),
(609, 'Hilarion', '', '', ''),
(610, 'Asterio', '', '', ''),
(611, 'Zotico', '20-04-92', 'MAR', 'TVE'),
(612, 'Dasio', '', '', ''),
(613, 'Atico', '', '', ''),
(614, 'Wicono', '', '', ''),
(615, 'Borromeo', '', '', ''),
(616, 'Apolinar', '', '', ''),
(617, 'Herculano', '01-03-92', 'SAN', 'SUR'),
(618, 'Amaranto', '', '', ''),
(619, 'Aquiles', '07-11-92', 'SAN', 'SUR'),
(620, 'Nicandro', '', '', ''),
(621, 'Millan', '', '', ''),
(622, 'Paterno', '', '', ''),
(623, 'Fuencisla', '', '', ''),
(624, 'Tifon', '', '', ''),
(625, 'Leon Magno', '', '', ''),
(626, 'Serapion', '21-03-92', 'SAN', 'SUR'),
(627, 'Veneranda', '', '', ''),
(628, 'Ramon Llul', '29-03-92', 'MAR', 'SUR'),
(629, 'Jonas', '29-03-92', 'SAN', 'TVE'),
(630, 'Leon', '01-03-92', 'SAN', 'SUR'),
(631, 'Albino', '01-03-92', 'SAN', 'SUR'),
(632, 'Juan Climaco', '30-03-92', 'ABA', 'TVE'),
(633, 'Quintin', '', '', ''),
(634, 'Quirino', '30-03-92', 'MAR', 'TVE'),
(635, 'Marino', '03-03-92', 'SAN', 'SUR'),
(636, 'Fortunato', '03-03-92', 'SAN', 'SUR'),
(637, 'Pablo el Simple', '07-03-92', 'SAN', 'SUR'),
(638, 'Pablo', '07-03-92', 'SAN', 'SUR'),
(639, 'Felicidad', '07-03-92', 'SAN', 'SUR'),
(640, 'Juan de Dios', '08-03-92', 'SAN', 'SUR'),
(641, 'Cirilo', '08-03-92', 'SAN', 'SUR'),
(642, 'Filemon', '08-03-92', 'SAN', 'SUR'),
(643, 'Apolonio', '08-03-92', 'SAN', 'SUR'),
(644, 'Poncio', '08-03-92', 'SAN', 'SUR'),
(645, 'Domingo Savio', '09-03-92', 'SAN', 'SUR'),
(646, 'Paciano', '09-03-92', 'SAN', 'SUR'),
(647, 'Cuadrado', '10-03-92', 'SAN', 'SUR'),
(648, 'Candido', '10-03-92', 'SAN', 'SUR'),
(649, 'Niceforo', '13-10-92', 'SAN', 'SUR'),
(650, 'Salomon', '13-03-92', 'SAN', 'SUR'),
(651, 'Marco', '13-03-92', 'SAN', 'SUR'),
(652, 'Silvano', '13-03-92', 'SAN', 'SUR'),
(653, 'Leon', '14-03-92', 'SAN', 'SUR'),
(654, 'Eutiquio', '14-03-92', 'SAN', 'SUR'),
(655, 'Arnaldo', '14-03-92', 'SAN', 'SUR'),
(656, 'Pedro', '14-03-92', 'SAN', 'SUR'),
(657, 'Alejandro', '14-03-92', 'SAN', 'SUR'),
(658, 'Evelina', '14-03-92', 'SAN', 'SUR'),
(659, 'Raimundo de Fitero', '15-03-92', 'SAN', 'SUR'),
(660, 'Sisebuto', '15-03-92', 'SAN', 'SUR'),
(661, 'Longinos', '15-03-92', 'SAN', 'SUR'),
(662, 'Aristobulo', '15-03-92', 'SAN', 'SUR'),
(663, 'Luisa de Marillac', '15-03-92', 'SAN', 'SUR'),
(664, 'Heriberto', '16-03-92', 'SAN', 'SUR'),
(665, 'Dionisio', '16-03-92', 'SAN', 'SUR'),
(666, 'Cirilo', '20-03-92', 'SAN', 'SUR'),
(667, 'Remigio', '20-03-92', 'SAN', 'SUR'),
(668, 'Filemon', '21-03-92', 'SAN', 'SUR'),
(669, 'Pablo', '22-03-92', 'SAN', 'SUR'),
(670, 'Deogracias', '22-03-92', 'SAN', 'SUR'),
(671, 'Saturnino', '22-03-92', 'SAN', 'SUR'),
(672, 'Reinalda', '22-03-92', 'SAN', 'SUR'),
(673, 'Alfonso de Mogrovejo', '23-03-92', 'SAN', 'SUR'),
(674, 'Jose Oriol', '23-03-92', 'SAN', 'SUR'),
(675, 'Benito', '23-03-92', 'SAN', 'SUR'),
(676, 'Dimas', '23-03-92', 'SAN', 'SUR'),
(677, 'Amadeo', '31-03-92', 'BEA', 'TVE'),
(678, 'Balbina', '31-03-92', 'VIR', 'TVE'),
(679, 'Felix', '31-03-92', 'SAN', 'TVE'),
(680, 'Teodulo', '31-03-92', 'SAN', 'TVE'),
(681, 'Francisco de Paula', '02-04-92', 'FUN', 'TVE'),
(682, 'Teodosio', '02-04-92', 'MAR', 'TVE'),
(683, 'Nicecio', '02-04-92', 'OBI', 'TVE'),
(684, 'Nicetas', '03-04-92', 'SAN', 'TVE'),
(685, 'Urbicio', '03-04-92', 'SAN', 'TVE'),
(686, 'Francisco Javier', '03-12-92', 'SAN', 'CAL'),
(687, 'Nicolas', '06-12-92', 'SAN', 'CAL'),
(688, 'Leocadia', '09-12-92', 'SAN', 'CAL'),
(689, 'Juana Fca.de Chantal', '12-12-92', 'SAN', 'CAL'),
(690, 'Juan de la Cruz', '14-12-92', 'SAN', 'CAL'),
(691, 'Adelaida', '16-12-92', 'SAN', 'CAL'),
(692, 'Urbano', '19-12-92', 'SAN', 'CAL'),
(693, 'Domingo de Silos', '20-12-92', 'SAN', 'CAL'),
(694, 'Pedro Canisio', '21-12-92', 'SAN', 'CAL'),
(695, 'Honorato', '22-12-92', 'SAN', 'CAL'),
(696, 'Juan Cancio', '23-12-92', 'SAN', 'CAL'),
(697, 'Juan Apostol Evang.', '27-12-92', 'SAN', 'CAL'),
(698, 'Tomas Becket', '29-12-92', 'SAN', 'CAL'),
(699, 'Simplicio', '02-03-92', 'SAN', 'CAL'),
(700, 'Francisca Romana', '09-03-92', 'SAN', 'CAL'),
(701, 'Anastasia', '10-03-92', 'SAN', 'CAL'),
(702, 'Madrona', '15-03-92', 'SAN', 'CAL'),
(703, 'Cirilo de Jerusalen', '18-03-92', 'SAN', 'CAL'),
(704, 'Heriberto', '20-03-92', 'SAN', 'CAL'),
(705, 'Sixto', '28-03-92', 'SAN', 'CAL'),
(706, 'Eustasio', '29-03-92', 'SAN', 'CAL'),
(707, 'Benito de Palermo', '04-04-92', 'CON', 'TVE'),
(708, 'Zosimo', '04-04-92', 'MAR', 'TVE'),
(709, 'Juan Bautista Salle', '07-04-92', 'SAN', 'TVE'),
(710, 'Donato', '07-04-92', 'MAR', 'TVE'),
(711, 'Edesio', '08-04-92', 'MAR', 'TVE'),
(712, 'Amancio', '08-04-92', 'SAN', 'TVE'),
(713, 'Perpetuo', '08-04-92', 'SAN', 'TVE'),
(714, 'Casilda', '09-04-92', 'SAN', 'TVE'),
(715, 'Acacio', '09-04-92', 'MAT', 'TVE'),
(716, 'Hilario', '09-04-92', 'MAR', 'TVE'),
(717, 'Procoro', '09-04-92', 'MAR', 'TVE'),
(718, 'Miguel de los Santos', '10-04-92', 'CON', 'TVE'),
(719, 'Macario', '10-04-92', 'MAR', 'TVE'),
(720, 'Apolonio', '10-04-92', 'MAR', 'TVE'),
(721, 'Gemma Galgani', '11-04-92', 'VIR', 'TVE'),
(722, 'Barsanufio', '11-04-92', 'MAR', 'TVE'),
(723, 'Antipas', '11-04-92', 'MAR', 'TVE'),
(724, 'Victor', '12-04-92', 'MAR', 'TVE'),
(725, 'Sabas', '12-04-92', 'MAR', 'TVE'),
(726, 'Basilio', '12-04-92', 'SAN', 'TVE'),
(727, 'Hermenegildo', '13-04-92', 'MAR', 'TVE'),
(728, 'Quintiniano', '13-04-92', 'MAR', 'TVE'),
(729, 'Maximo', '14-04-92', 'SAN', 'TVE'),
(730, 'Valeriano', '14-04-92', 'MAR', 'TVE'),
(731, 'Lamberto', '14-04-92', 'OBI', 'TVE'),
(732, 'Anastasia', '15-04-92', 'SAN', 'TVE'),
(733, 'Eutilio', '15-04-92', 'MAR', 'TVE'),
(734, 'Basilisa', '15-04-92', 'MAR', 'TVE'),
(735, 'Crescente', '15-04-92', 'MAR', 'TVE'),
(736, 'Maximo', '15-04-92', 'SAN', 'TVE'),
(737, 'Toribio de Liebana', '16-04-92', 'OBI', 'TVE'),
(738, 'Elias', '17-04-92', 'MAR', 'TVE'),
(739, 'Roberto', '17-04-92', 'CON', 'TVE'),
(740, 'Esteban', '17-04-92', 'CON', 'TVE'),
(741, 'Expedito', '19-04-92', 'MAR', 'TVE'),
(742, 'Dionisio', '19-04-92', 'MAR', 'TVE'),
(743, 'Cayo', '19-04-92', 'MAR', 'TVE'),
(744, 'Vicente', '19-04-92', 'MAR', 'TVE'),
(745, 'Timon', '19-04-92', 'MAR', 'TVE'),
(746, 'Galacio', '19-04-92', 'MAR', 'TVE'),
(747, 'Severiano', '20-04-92', 'MAR', 'TVE'),
(748, 'Victor', '20-04-92', 'MAR', 'TVE'),
(749, 'Crisogono', '20-04-92', 'MAR', 'TVE'),
(750, 'Antonino', '20-04-92', 'MAR', 'TVE'),
(751, 'Cesareo', '20-04-92', 'MAR', 'TVE'),
(752, 'Jose', '22-04-92', 'MAR', 'TVE'),
(753, 'Parmenio', '22-04-92', 'MAR', 'TVE'),
(754, 'Lucio', '22-04-92', 'MAR', 'TVE'),
(755, 'Leon', '22-04-92', 'OBI', 'TVE'),
(756, 'Felix', '23-04-92', 'OBI', 'TVE'),
(757, 'Fortunato', '23-04-92', 'OBI', 'TVE'),
(758, 'Adalberto', '23-04-92', 'OBI', 'TVE'),
(759, 'Calixta', '25-04-92', 'MAR', 'TVE'),
(760, 'Euodio', '25-04-92', 'MAR', 'TVE'),
(761, 'Isidoro', '26-04-92', 'MAR', 'TVE'),
(762, 'NtraSra.Buen Concejo', '26-04-92', '', 'TVE'),
(763, 'Basilio', '26-04-92', 'MAR', 'TVE'),
(764, 'Pedro', '26-04-92', 'MAR', 'TVE'),
(765, 'Cleto', '26-04-92', 'MAR', 'TVE'),
(766, 'Honesta', '19-10-92', 'SAN', 'D16'),
(767, 'Leptina', '26-10-92', 'SAN', 'SUR'),
(768, 'Florencio', '07-11-92', 'SAN', 'SUR'),
(769, 'Rufo', '07-11-92', 'SAN', 'SUR'),
(770, 'Severino', '07-11-92', 'SAN', 'SUR'),
(771, 'Gertrudis', '07-11-92', 'SAN', 'SUR'),
(772, 'NSra. de la Almudena', '09-11-92', 'VIR', 'SUR'),
(773, 'Teodoro', '09-11-92', 'SAN', 'SUR'),
(774, 'Alejandro', '09-11-92', 'SAN', 'SUR'),
(775, 'Agripino', '09-11-92', 'SAN', 'SUR'),
(776, 'Benigno', '09-11-92', 'SAN', 'SUR'),
(777, 'Oreste', '09-11-95', 'SAN', 'D16'),
(778, 'Iluminada', '29-11-92', 'SAN', 'D16'),
(779, 'Castulo', '30-11-95', 'SAN', 'D16'),
(780, 'Justina', '30-11-95', 'SAN', 'D16'),
(781, 'Maura', '30-11-95', 'SAN', 'D16'),
(782, 'Melanio', '07-01-92', 'SAN', 'ABC'),
(783, 'Nilamon', '07-01-96', 'SAN', 'ABC'),
(784, 'Anatolio', '07-01-96', 'SAN', 'ABC'),
(785, 'Liceria', '07-01-96', 'SAN', 'ABC'),
(786, 'Focas', '05-03-97', 'SAN', 'CLM');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
