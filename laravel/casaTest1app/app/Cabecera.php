<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cabecera extends Model
{
    protected $table='testcabe';

    protected $primaryKey = 'id';

    protected $fillable =  array('id_santoral', 'nombre');
}
