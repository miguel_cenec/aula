<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Linea extends Model
{
    protected $table='testline';

    protected $primaryKey = 'id_cabe;nro_linea';

    protected $fillable =  array('fecha', 'nombre', 'nentero', 'ndecimal','sino','cbstr','cbint','cbtbl');
}
