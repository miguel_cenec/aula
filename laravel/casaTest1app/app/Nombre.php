<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nombre extends Model
{
    protected $table='santoral';

    protected $primaryKey = 'id';

    protected $fillable =  array('nombre', 'dia', 'tipo', 'fuente');
}
