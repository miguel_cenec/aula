<?php

Route::get('/', function () { 
	return view('Welcome'); 
}); 

Route::get('/usuarios', function () { 
	return 'Hola usuarios'; 
});

Route::get('/usuarios/detalles', function () { 
	return 'Mostrando detalle:'.$_GET'id'; 
});

Route::get('/usuarios/{id}', function($id) { 
	return "Mostrando detalle:$id"; })->where('id','0-9+');

//probar que funcxiones cambiando $x por $id									// PDTE
Route::get('/usuarios/{x}', function($x) { 
	return "Crear usuario nuevo"; 
});

Route::get('/saludo/{name}/{nickname?}', function($name,$nickname=null) { 
	$name=ucfirst($name); 
	if ($nickname) 
		return "Bienvenido {$name},tu apodo es {$nickname}"; 
	else return "Bienvenido {$name},no tienes apodo"; 
})
