<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Test</title>
  </head>
  <body>
    <h1>This is a test</h1>

    @yield('yld1')
    @yield('yld2', 'padre yld2-alt')
    @yield('yld3', '<p>padre yld3-alt</p>')
    @yield('yld4', 'padre yld4')

    @section('sct1')
    @show

    @section('sct2')
      padre sct2
    @show

    @section('sct3')
      <p>padre sct3</p>
    @show

    @section('sct4')
      <p>padre sct4</p>
    @show


  </body>
</html>