@extends('miblade/bladePadre')

@section('yld4')
  <div>
    <p>Hija seccion(yld4)</p>
    <hr>
  </div>
@endsection

@section('sct3')
  <div>
    <p>Hija seccion(sct3)</p>
    <hr>
  </div>
@endsection

@section('sct4')
  @parent
  <div>
    <p>Hija seccion(sct4)</p>
    <hr>
  </div>
@endsection

@section('sct2')
  @component('miblade/alerta1')
      @slot('title')
          Forbidden
      @endslot

      You are not allowed to access this resource!
  @endcomponent
@endsection
