<?php

namespace CursoCenec\Http\Controllers;
//namespace App\Http\Controllers;


use Illuminate\Http\Request;

class CatalogoController extends Controller
{
	public function getIndex() {
		
		$pelis=['Luis','Alberto','Laura','Julia','Ana','<script>alert("Click")</script>'];		
		$genero = 'miedo';


		// ok (compac hace los de abaj0p, convierte variables en array)
		return view('index',compact('genero','pelis'));

		// funciona ok
		//return view('index',['pelis'=> $pelis,	'genero'=>$genero]);


		/*
		// funciona ok
		return view('index')
		->with('pelis', $pelis)
		->with('genero', $genero);
		*/
		

		//return view('index');
	}

	public function getShow($id) {
		return view('show', array('id'=>$id));
	}
	
	public function checkMD() {
            return redirect('/ej38');
	}
		
/*	
	// solo devuelven vista
	public function getCreate() {

	}
	public function getEdit() {

	}
*/

}
