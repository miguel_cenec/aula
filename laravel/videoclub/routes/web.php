<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/ej38', function () {
    return view('layaouts/master');
});

Route::get('/login', function () {
    return view('login');
});



Route::get('/catalogo', 'CatalogoController@getIndex');
/*
Route::get('/catalogo', function () {
    return view('index');
});
*/

Route::get('/catalogo/show/{id}', 'CatalogoController@getShow');
/*
Route::get('/catalogo/show/{id}', function ($id) {
	//return view('show', array('id'=>$id));
	
});
*/

Route::get('/catalogo/create', function () {
    return view('create');
});

Route::get('/catalogo/edit/{id}', function ($id) {
	return view('edit', array('id'=>$id));    
});

Route::group(['prefix' => 'blade'], function () {
    Route::get('padre', function () { return view('miblade/bladePadre'); });
    Route::get('hija', function () { return view('miblade/bladeHija'); });
});

Route::get('/saludo/{name}/{nickname?}', function($name,$nickname=null) { 
	$name=ucfirst($name); 
	if ($nickname) 
		return "Bienvenido {$name},tu apodo es {$nickname}"; 
	else 
		return "Bienvenido {$name},no tienes apodo";
});	


Route::get('/enmedio', function () { return view('varias.enmedio'); });
Route::get('/enmedio-chk',['uses'=>'CatalogoController@checkMD','middleware'=>'enmedioMD']);


/*
// erro:
// "Route [nombres.index] not defined. (View: C:\xampp\htdocs\laravel\lara1\resources\views\nombres\index.blade.php)"
// parece que si tiene prefijo no ha manera de acceder a nombres con ruta '/'

Route::group(['prefix' => 'nombres'], function () {
	// tabla bootstrap con filtros mejorados
	Route::get('nombresF1', 'NombreController@filtro1');
	Route::resource('/', 'NombreController');
});

*/

Route::get('nombresF1', 'NombreController@filtro1');
Route::resource('nombres', 'NombreController');



/*
Route::get('/login', function () {
    return 'login usuario';
});

Route::get('/logout', function () {
    return 'logout usuario';
});

Route::get('/catalog', function () {
    return 'lista peliculas';
});

Route::get('/catalog/show/{id}', function ($id) {
    return 'vista pelicula'.$id;
});

Route::get('/catalog/create', function () {
    return view('Añadir pelicula');
});

Route::get('/catalog/edit/{id}', function ($id) {
    return 'modificar pelicula'.$id;
});
*/
