@extends('layaouts.master')

<!-- Bootstrap Core CSS 
@section('mis_estilos')
<link media="all" type="text/css" rel="stylesheet" href="/assets/bootstrap-3.3.7/css/bootstrap.css">
@stop
-->

@section('content')
<h1>
  nombre {{ $nombre->nombre}}
</h1>
 
<p>Id nombre; {{ $nombre->id}}</p>
<p>nombre: {{ $nombre->nombre}}</p>
<p>dia: {{ $nombre->dia }}</p>
<p>tipo: {{ $nombre->tipo}}</p>
<p>fuente:{{ $nombre->fuente}}</p>
<hr>
 
<a href="{{ route('nombres.index') }}" class="btn btn-info">Volver al índice</a>
<a href="{{ route('nombres.show', $nombre->id) }}" class="btn btn-info">Recargar</a>
@stop

