@extends('app')
@section('mis_estilos')
<!-- Bootstrap Core CSS -->
<link media="all" type="text/css" rel="stylesheet" href="/assets/bootstrap-3.3.7/css/bootstrap.css">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<style>
.vcenter {
    display: inline-block;
    vertical-align: middle;
    float: none;
}
</style>
@stop
@section('content')
<h1 class="text-primary">Lista de Nombres</h1>

<div class="row">
  <div class="col-sm-4 vcenter">
    <a href="nombres?filtro1=A">A</a> |
    <a href="nombres?filtro1=E">E</a> |
    <a href="nombres?filtro1=I">I</a> |
    <a href="nombres?filtro1=O">O</a> |
    <a href="nombres?filtro1=U">U</a> |
    <a href="nombres">Reset</a>
  </div>
  <div class="col-sm-4">
    <form method="GET" action="nombres">
      <input type="text" name="search" placeholder="Search..." value ="{{ app('request')->input('search') }}">
        <button class="btn btn-default-sm" type="submit">
          <i class="fa fa-search" aria-hidden="true"></i>
        </button>
    </form>
  </div>
  <div class="col-sm-3 vcenter text-right">
    <a href="nombres?sort=asc">Ascend.</a> |
    <a href="nombres?sort=desc">Descen.</a> 
  </div>
</div>

<table class="table table-bordered table-striped table-hover" id="tableNombres">
  <thead>
    <tr>
        <th class="text-center">Id</th>
        <th class="text-center">Nombre</th>
        <th class="text-center">Dia</th>
        <th class="text-center">Tipo</th>
        <th class="text-center">Fuente</th>
        <th class="text-center">Acciones</th>
    </tr>
  </thead>
  <tbody>
    @foreach($nombres as $dato)
        <tr>
            <td class="text-center">{{ $dato->id}}</td>
            <td class="text-center">{{ $dato->nombre}}</td>
            <td class="text-center">{{ $dato->dia }}</td>
            <td class="text-center">{{ $dato->tipo}}</td>
            <td class="text-center">{{ $dato->fuente}}</td>
            <td>
                <a href="{{ route('nombres.show', $dato->id) }}"><i class="fa fa-search" aria-hidden="true"></i></a>
            </td>
        </tr>
    @endforeach
  </tbody>
</table>
{{ $nombres->links() }} 
<br>
Records {{ $nombres->firstItem() }} - {{ $nombres->lastItem() }} of {{ $nombres->total() }} (for page {{ $nombres->currentPage() }} de {{ $nombres->lastPage() }}  )
@stop

@section('app_scripts')
  <script src="/assets/jquery-3.2.1.min.js"></script>
  <script src="/assets/bootstrap-3.3.7/js/bootstrap.min.js"></script>
@stop
