 <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Video Club</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="{{url('/ej38')}}">Inicio</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Catalogo
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="{{url('/catalogo')}}">Indice</a></li>
          <li><a href="{{url('/catalogo/create')}}">Nueva</a></li>
          <li><a href="{{url('/catalogo/show/peli2')}}">Mostrar</a></li>          
          <li><a href="{{url('/catalogo/edit/peli1')}}">Editar</a></li>
        </ul>
      </li>

      <li><a href="#">Page 3</a></li>

      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Otros
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="{{url('/saludo/Juan')}}">Sin apodo</a></li>
          <li><a href="{{url('/saludo/Jose/Nadie')}}">Con apodo</a></li>
          <li><a href="{{url('/enmedio')}}">Middleware</a></li>
        </ul>
      </li>

      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Blade
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="{{url('/blade/padre')}}">Padre</a></li>
          <li><a href="{{url('/blade/hija')}}">Hija</a></li>
        </ul>
      </li>


    </ul>



	<ul class="nav navbar-nav navbar-right">
		<li><a href="{{url('/login')}}">Login</a></li>
	</ul>



    
  </div>
</nav> 
