@extends('layaouts.master')

@section('content')
	<h1>Catalogo de Peliculas {{ $genero }}</h1>

<hr>

	@empty($pelis)
		<p>No hay usuarios registrados.</p>
	@else
		<?php foreach($pelis as $peli):?>
			<ul>
				<li> <?= e($peli) ?></li>
			</ul>      
		<?php endforeach;?>
	@endempty
<br>	
Hora:{{time()}}
@stop
