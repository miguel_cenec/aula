<?php

namespace CursoCenec\Http\Middleware;
//namespace App\Http\Middleware;

use Closure;

class EnmedioMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->nombre != 'miguel') {
            return redirect('/login');
        }
        		
		return $next($request);
    }
}
