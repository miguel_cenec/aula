<?php
/*
La ruta de este archivo es:
C:\xampp\htdocs\laravel\lara1\tests\Feature
*/ 
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WelcomeTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasic1Test()
    {
        $response = $this->get('/saludo/luis/moreno');

        //$response->assertStatus(200);
        $response->assertSee("Bienvenido Luis,tu apodo es moreno");
    }
}
