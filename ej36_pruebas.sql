-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-10-2018 a las 10:43:49
-- Versión del servidor: 10.1.35-MariaDB
-- Versión de PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pruebas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anticuarios`
--

CREATE TABLE `anticuarios` (
  `id_anticuario` int(11) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `anticuarios`
--

INSERT INTO `anticuarios` (`id_anticuario`, `apellidos`, `nombre`) VALUES
(1, 'Jones', 'Bill'),
(2, 'Smith', 'Bob'),
(15, 'Lawson', 'Patricia'),
(21, 'Akins', 'Jane'),
(50, 'Fowler', 'Sam');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `antiguedades`
--

CREATE TABLE `antiguedades` (
  `id_vendedor` int(11) NOT NULL,
  `id_comprador` int(11) NOT NULL,
  `objeto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `antiguedades`
--

INSERT INTO `antiguedades` (`id_vendedor`, `id_comprador`, `objeto`) VALUES
(1, 15, 'Joyero'),
(1, 21, 'Vitrina'),
(1, 50, 'Cama'),
(2, 15, 'Mesa'),
(2, 21, 'Jarron'),
(2, 21, 'Mesa de Cafe'),
(15, 2, 'Silla'),
(15, 50, 'Silla'),
(21, 2, 'Libreria'),
(21, 50, 'Espejo'),
(50, 1, 'Escritorio'),
(50, 1, 'Perchero');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ord_compra`
--

CREATE TABLE `ord_compra` (
  `id_anticuario` int(11) NOT NULL,
  `objeto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ord_compra`
--

INSERT INTO `ord_compra` (`id_anticuario`, `objeto`) VALUES
(2, 'Mesa'),
(2, 'Escritorio'),
(21, 'Silla'),
(15, 'Espejo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `precios`
--

CREATE TABLE `precios` (
  `objeto` varchar(50) NOT NULL,
  `precio` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `precios`
--

INSERT INTO `precios` (`objeto`, `precio`) VALUES
('Cama', '24'),
('Escritorio', '301'),
('Espejo', '48'),
('Jarron', '54'),
('Joyero', '30'),
('Libreria', '210'),
('Mesa', '150'),
('Mesa de Cafe', '90'),
('Perchero', '45'),
('Silla', '39'),
('Vitrina', '120');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `anticuarios`
--
ALTER TABLE `anticuarios`
  ADD PRIMARY KEY (`id_anticuario`),
  ADD KEY `Iantc1` (`nombre`);

--
-- Indices de la tabla `antiguedades`
--
ALTER TABLE `antiguedades`
  ADD PRIMARY KEY (`id_vendedor`,`id_comprador`,`objeto`) USING BTREE,
  ADD KEY `Iantg1` (`id_comprador`),
  ADD KEY `Iantg2` (`id_vendedor`),
  ADD KEY `Iantg3` (`objeto`);

--
-- Indices de la tabla `ord_compra`
--
ALTER TABLE `ord_compra`
  ADD PRIMARY KEY (`id_anticuario`,`objeto`) USING BTREE,
  ADD KEY `Iordc1` (`id_anticuario`),
  ADD KEY `Iordc2` (`objeto`);

--
-- Indices de la tabla `precios`
--
ALTER TABLE `precios`
  ADD PRIMARY KEY (`objeto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `anticuarios`
--
ALTER TABLE `anticuarios`
  MODIFY `id_anticuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `antiguedades`
--
ALTER TABLE `antiguedades`
  ADD CONSTRAINT `fkantig1` FOREIGN KEY (`objeto`) REFERENCES `precios` (`objeto`),
  ADD CONSTRAINT `fkantig2` FOREIGN KEY (`id_comprador`) REFERENCES `anticuarios` (`id_anticuario`),
  ADD CONSTRAINT `fkantig3` FOREIGN KEY (`id_vendedor`) REFERENCES `anticuarios` (`id_anticuario`);

--
-- Filtros para la tabla `ord_compra`
--
ALTER TABLE `ord_compra`
  ADD CONSTRAINT `fkordc1` FOREIGN KEY (`id_anticuario`) REFERENCES `anticuarios` (`id_anticuario`),
  ADD CONSTRAINT `fkordc2` FOREIGN KEY (`objeto`) REFERENCES `precios` (`objeto`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
