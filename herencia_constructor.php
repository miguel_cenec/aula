<?php
/*
http://localhost:8088/cnc/herencia_constructor.php

- no funcionan igual que java
 
- se heredan si hijo no define uno, subira hasta encontrar uno
si hijo define uno solo se ejecutara este  
  
*/

Class Padre {
	public $padrev = "lalal<br>";

	function __construct() { echo "constructor Padre<br>"; }
}	


Class Hijo extends Padre {

	function __construct() { echo "constructor Hijo<br>";}
	
	function verv1() { echo $this->padrev;	}		
}	

Class Nieto extends Hijo {

	function __construct() { echo "constructor nieto<br>";}
	
	function verv1() { echo $this->padrev;	}		
}	


?>
<html>
<head>
	<meta charset="UTF-8">
	<title>Ejer. -ej_34-</title>
	

<style>
</style>

<script>
</script>


</head>

<body>


<?php

	$o1 = new Hijo();
	
	$o1->verv1();

	$o1 = new Nieto();

		
?>


</body>
</html>
