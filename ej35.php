<?php
/*
http://localhost:8088/cnc/ej35.php
*/

Abstract Class Trabajador {
	public $nombre;
	public $sueldo;

	abstract function calcularSueldo();

	function verBasicos() {
		$txt = 'nombre: '.$nombre.'<br>';
		$txt .= 'sueldo: '.$sueldo.'<br>';
		return $txt;
	}	

}	

Class Empleado extends Trabajador {
	
	public $horasTrabajadas;
	private $valorHora = 3.5;

	function calcularSueldo() {
		return $this->horasTrabajadas*$this->valorHora;
	}	
}	

Class Gerente extends Empleado {

	function calcularSueldo() { 
		$base = parent::calcularSueldo();
		$base = $base + ($base*10/100);
		return $base;
	}		
}	


?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Ejer. -ej_34-</title>
	

<style>
</style>

<script>
</script>


</head>

<body>


<?php

	$o1 = new Gerente();
	$o1->horasTrabajadas = 40;

	echo 'Horas trabajadas:'.$o1->horasTrabajadas.'<br>';
	
	echo 'sueldo del gerente:'.$o1->calcularSueldo();


		
?>


</body>
</html>

