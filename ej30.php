<?php
/*
http://localhost:8088/cnc/ej30.php 
*/

class Menu {

	private $opciones = array();	
	
	public function addOption($valor) {
		$this->opciones[] = $valor;	
	}		

	public function showHor() {
		echo $this->formatLink(0);		
	}	

	public function showVer() {
		echo $this->formatLink(1);
	}	


	private function formatLink($estilo) {
		
		$txt = '';
		$sep = ($estilo == 0) ? '<br>' : '&nbsp;&nbsp;&nbsp;';

		foreach ($this->opciones as $op) {
			$txt .= '<a href="'.$op[1].'">'.$op[0].'</a>'.$sep;
		}	
		
		return $txt; 	
	}	

}	
			
?>

<html>
<head>
	<meta charset="UTF-8">
	<title>Ejer. -ej_30-</title>
	

<style>
</style>

<script>
</script>


</head>

<body>


<?php

	$opci = [ ['google','http://google.es'], ['yahoo','http://yojoo.es'], ['Msn','http://msn.es'] ];
	//echo '<pre>';print_r($opci);'</pre><hr>';


	$misops = new Menu();

	foreach ($opci as $op) {
		$misops->addOption($op);
	}	


	$misops->showHor();
	echo '<hr>';
	$misops->showVer();			
?>


</body>
</html>

