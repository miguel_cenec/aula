<?php
/*
http://localhost:8088/cnc/ej32.php 
*/

Class Operacion {

	protected $valor1;
	protected $valor2;
	
	function __construct($v1, $v2) {
		$this->valor1 = $v1;		
		$this->valor2 = $v2;		
	}

	public function show() {}		
}
	
Class Suma extends Operacion {

	function __construct($v1,$v2) {
	   parent::__construct($v1,$v2);
	}	

	public function show() {
		return ($this->valor1+$this->valor2);	
	}	
}		

Class Resta extends Operacion {

	function __construct($v1,$v2) {
	   parent::__construct($v1,$v2);
	}	
	
	public function show() {
		return $this->valor1-$this->valor2;	
	}	
}		
	
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Ejer. -ej_32-</title>
	

<style>
</style>

<script>
</script>


</head>

<body>

<?php

$v1 = mt_rand(0, 99);
$v2 = mt_rand(0, 99);

echo 'Valor 1: '.$v1.'<br>';
echo 'Valor 2: '.$v2.'<br><br><br>';

$o1 = new Suma($v1, $v2);
$o2 = new Resta($v1, $v2);



echo 'la suma es:'. $o1->show(); 
echo '<br><br><br>';
echo 'la resta es:'. $o2->show();


?>
 <br><br><br>
 <button onclick="window.location.reload()">Probar otra vez</button> 
 
</body>
</html>

